## An Android app for automatic facial retouching.

This is a repo created to show my old Android application I've created at the end of 2017 as part of my engineer thesis at the UTP WTiE.

The application contains a number of original programming solutions that can be applied in more complex implementations encompassing advanced, realistic, automatic digital image editing on a mobile device, involving face landmark recognition.

You can find a very detailed description of the code (in Polish) in my paper available [here](WJ_TIN_NS_WTIiE_Lite.pdf).

The main body of the paper includes a description of the used face landmark detection method, as well as a description of the implemented methods used for digital image correction. The first of those methods uses a dlib C++ via JNI to Android SDK library port combined with elements of OpenCV, while the second implements the OpenCV library for purposes of digital image correction. Both descriptions include references to the most important fragments of the attached source code. Beyond that, the paper includes: a short characteristic of possible practical applications of the proposed solutions, a characteristic of the Android SDK, a description of the used libraries and app’s structure.
Keywords: face recognition, OpenCV, dlib, Android, Java, facial landmarks, mobile application, digital image, manipulation , correction. Both descriptions include references to the most important fragments of the attached source code. Beyond, the paper includes: a short characteristic of possible practical applications of the proposed solutions, a characteristic of the Android SDK, a description of the used libraries and app’s structure.

## Precompiled .APK
You can also download the precompiled .apk [here](app-debug.apk).

## Auto retouch examples

![](/Zdjecia/00.jpg)

![](/Zdjecia/26.png)

![](/Zdjecia/15.jpg)

![](/Zdjecia/4.jpg)

## Important
In case you're encountering a gradle script error during compilation, make sure if this isn't caused by a too long file path to your project files. In that case just place the project closer to the root.

## License
[MIT](https://choosealicense.com/licenses/mit/)
