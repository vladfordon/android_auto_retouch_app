package com.jas.wmj.filterTextureData;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

import com.jas.wmj.wmjPhotoApp.R;

import java.util.ArrayList;

/**
 * Created by WMJ on 2017-06-01.
 */


public class AccesoryTextureData {

    private Context _context;

    public AccesoryTextureData(Context context){
        _context = context;
    }

    public ArrayList<Point> aBeardPoints(){

        ArrayList<Point> xpoints = new ArrayList<>();
        xpoints.add( new Point(113,507) );
        xpoints.add( new Point(115,573) );
        xpoints.add( new Point(124,640) );
        xpoints.add( new Point(139,707) );
        xpoints.add( new Point(162,771) );
        xpoints.add( new Point(197,826) );
        xpoints.add( new Point(241,874) );
        xpoints.add( new Point(297,908) );
        xpoints.add( new Point(363,915) );
        xpoints.add( new Point(427,907) );
        xpoints.add( new Point(482,872) );
        xpoints.add( new Point(528,822) );
        xpoints.add( new Point(562,764) );
        xpoints.add( new Point(580,699) );
        xpoints.add( new Point(588,630) );
        xpoints.add( new Point(589,562) );
        xpoints.add( new Point(582,496) );
        xpoints.add( new Point(151,477) );
        xpoints.add( new Point(180,444) );
        xpoints.add( new Point(224,433) );
        xpoints.add( new Point(271,435) );
        xpoints.add( new Point(315,449) );
        xpoints.add( new Point(372,441) );
        xpoints.add( new Point(416,425) );
        xpoints.add( new Point(464,420) );
        xpoints.add( new Point(508,432) );
        xpoints.add( new Point(537,465) );
        xpoints.add( new Point(348,492) );
        xpoints.add( new Point(350,541) );
        xpoints.add( new Point(351,590) );
        xpoints.add( new Point(353,640) );
        xpoints.add( new Point(303,666) );
        xpoints.add( new Point(327,673) );
        xpoints.add( new Point(354,681) );
        xpoints.add( new Point(381,672) );
        xpoints.add( new Point(407,665) );
        xpoints.add( new Point(203,501) );
        xpoints.add( new Point(228,483) );
        xpoints.add( new Point(260,480) );
        xpoints.add( new Point(290,499) );
        xpoints.add( new Point(260,506) );
        xpoints.add( new Point(229,509) );
        xpoints.add( new Point(406,498) );
        xpoints.add( new Point(434,478) );
        xpoints.add( new Point(465,477) );
        xpoints.add( new Point(490,494) );
        xpoints.add( new Point(467,504) );
        xpoints.add( new Point(437,503) );
        xpoints.add( new Point(265,753) );
        xpoints.add( new Point(299,741) );
        xpoints.add( new Point(333,733) );
        xpoints.add( new Point(357,739) );
        xpoints.add( new Point(380,732) );
        xpoints.add( new Point(416,738) );
        xpoints.add( new Point(454,750) );
        xpoints.add( new Point(420,771) );
        xpoints.add( new Point(385,778) );
        xpoints.add( new Point(359,780) );
        xpoints.add( new Point(333,779) );
        xpoints.add( new Point(299,772) );
        xpoints.add( new Point(278,754) );
        xpoints.add( new Point(333,750) );
        xpoints.add( new Point(358,754) );
        xpoints.add( new Point(382,750) );
        xpoints.add( new Point(443,751) );
        xpoints.add( new Point(382,749) );
        xpoints.add( new Point(358,753) );
        xpoints.add( new Point(333,750) );
        return xpoints;
    }
    public Bitmap aBeardTexture(){ return BitmapFactory.decodeResource(_context.getResources(), R.drawable.a_full_beard_and_hair_texture); }
   // public Bitmap aBeardMask(){ return BitmapFactory.decodeResource(_context.getResources(), R.drawable.a_full_beard_and_hair_mask); }

}
