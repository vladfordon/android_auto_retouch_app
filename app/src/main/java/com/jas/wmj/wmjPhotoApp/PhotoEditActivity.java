package com.jas.wmj.wmjPhotoApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.jas.wmj.Filters.AverageBitmap;
import com.jas.wmj.appUtils.CapturePhotoUtils;
import com.jas.wmj.appUtils.DlibHelper;
import com.jas.wmj.appUtils.Enums;
import com.jas.wmj.appUtils.FileUtils;
import com.jas.wmj.appUtils.MainSingleton;
import com.jas.wmj.appUtils.filterHelpers.WMJFaceImage;
import com.jas.wmj.filterTextureData.AccesoryTextureData;
import com.tzutalin.dlib.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by WMJ on 2016-12-12.
 */


public class PhotoEditActivity extends Activity{

    //<editor-fold desc="Zmienne globalne">
    private SubsamplingScaleImageView photoImage;
    private View bcgView;
    private int buttonDelay = 415;

    private Bitmap outputImage;

    public ImageButton f1;
    public ImageButton f2;
    public ImageButton f3;
    public ImageButton f4;

    public Button noneButton;

    public View lowerButtonsLinearLayout;

    public Button standardTextureButton;
    public Button strongTextureButton;
    public Button accButton;
    public Button debugButton;


    private HorizontalScrollView filterMenu;
    private static GestureDetector gestureDetector;

    private TextToSpeech mTts;
    private boolean originalImageLandmarksFound = false;

    private boolean didCreateTemp = false;

    boolean filter_isActive = false;

    private boolean addNormalTexture = true;
    private boolean addAbeard = false;
    private boolean debugMode = false;


    //---------------------------------- tryb prezentacji -------
    boolean isInPresentationMode = true;
    //-----------------------------------------------------------

    //</editor-fold>

    //-----------------------------------------

    //<editor-fold desc="Cykl życia">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_edit);

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            loadPhoto();
            loadUI();
            loadTTSengine();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mTts == null) {
            mTts = new TextToSpeech(getBaseContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    mTts.setLanguage(new Locale("pl_PL"));
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mTts != null){mTts.shutdown();}
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mTts != null){mTts.shutdown();}
    }

    public void onDestroy() {
        super.onDestroy();
        if (mTts != null){mTts.shutdown();}
    }
    //</editor-fold>

    //<editor-fold desc="Obsługa UI">
    private void loadUI() {

        filterMenu = (HorizontalScrollView) findViewById(R.id.filterMenu);
        f1 = (ImageButton)findViewById(R.id.f1);
        f2 = (ImageButton)findViewById(R.id.f2);
        f3 = (ImageButton)findViewById(R.id.f3);
        f4 = (ImageButton)findViewById(R.id.f4);

        lowerButtonsLinearLayout = findViewById(R.id.linearLayout2);
        noneButton = (Button)findViewById(R.id.noneButton);

        standardTextureButton = (Button)findViewById(R.id.standardTexture);
        strongTextureButton = (Button)findViewById(R.id.strongTexture);
        accButton = (Button)findViewById(R.id.accButton);

        debugButton = (Button)findViewById(R.id.debugButton);

        f1.setImageResource(R.drawable.f_defman_texture);
        f2.setImageResource(R.drawable.f_defwoman_texture);
        f3.setImageResource(R.drawable.f_blackman_texture);
        f4.setImageResource(R.drawable.f_asianman_texture);

        if (isInPresentationMode){
            accButton.setVisibility(View.GONE);
        }

        updateButtons();
    }

    private void updateButtons(){

        if (addNormalTexture){
            standardTextureButton.setBackground(getDrawable( R.drawable.ripple_effect_black_11x ));
            strongTextureButton.setBackground(getDrawable( R.drawable.ripple_effect_black_4x ));
        }
        else
        {
            standardTextureButton.setBackground(getDrawable( R.drawable.ripple_effect_black_3x ));
            strongTextureButton.setBackground(getDrawable( R.drawable.ripple_effect_black_11x ));
        }

            if (!isInPresentationMode){
                if (addAbeard){ accButton.setBackground(getDrawable( R.drawable.ripple_effect_black_11x )); }
                else { accButton.setBackground(getDrawable( R.drawable.ripple_effect_black_2x )); }
            }

        if (debugMode){ debugButton.setBackground(getDrawable( R.drawable.ripple_effect_black_11x )); }
        else { debugButton.setBackground(getDrawable( R.drawable.ripple_effect_black_2x )); }

    }

    public void newPhoto(View v) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
           public void run() {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (mTts != null){mTts.shutdown();}
                        Intent intent = new Intent(PhotoEditActivity.this, CameraCalibrationActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
           }
        }, buttonDelay);
    }

    private void showFilterOptions() {

        showOriginalImage();
        filter_isActive = false;

        filterMenu.setVisibility(View.VISIBLE);
        standardTextureButton.setVisibility(View.VISIBLE);
        strongTextureButton.setVisibility(View.VISIBLE);

                if (!isInPresentationMode){
                accButton.setVisibility(View.VISIBLE);}

        debugButton.setVisibility(View.VISIBLE);
        lowerButtonsLinearLayout.setVisibility(View.GONE);


    }

    private void hideFilterMenu() {

        filterMenu.setVisibility(View.INVISIBLE);
        standardTextureButton.setVisibility(View.INVISIBLE);
        strongTextureButton.setVisibility(View.INVISIBLE);

                if (!isInPresentationMode){
                accButton.setVisibility(View.INVISIBLE);}

        debugButton.setVisibility(View.INVISIBLE);
        lowerButtonsLinearLayout.setVisibility(View.VISIBLE);


    }

   public void savePhoto(View v) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        Calendar c = Calendar.getInstance();
                        int seconds = c.get(Calendar.SECOND);
                        String title = "WMJ"+seconds;

                        CapturePhotoUtils.insertImage(getContentResolver(), outputImage, title,"wmj-edited");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() { Toast.makeText(getApplicationContext(), getResources().getString(R.string.photoSaved_Toast), Toast.LENGTH_LONG).show();}});

                    } });

            }
        }, buttonDelay);
    }
    //</editor-fold>

    //<editor-fold desc="Ladowanie i zapisywanie zdjęcia">
    private void loadPhoto() {

        bcgView = findViewById(R.id.bcgView);
        photoImage = (SubsamplingScaleImageView) findViewById(R.id.imageView);

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (photoImage.isReady()) {
                    showFilterOptions();
                }
                return true;
            }
        });

        photoImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
        });

            if (MainSingleton.getInstance().getmRGBbitmap() != null) {

                // implementacja davemorrissey/subsampling-scale-image-view
                photoImage.setOrientation(SubsamplingScaleImageView.ORIENTATION_0);
                photoImage.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CENTER_CROP);

                //Ladowanie z pamięci ortginalne zdjęcie, zapisuje je w pliku wymiany i ponownie ładuje je z pliku wymiany (kiedy zakończyło się tworzenie pliku wymiany)
                createTempFile();

                //Odczyt landmarkow oryginalnego obrazu z poprzedniego wdoku
                if (MainSingleton.getInstance().getOriginalImageLandmarks() != null &&
                        MainSingleton.getInstance().getOriginalImageLandmarks().size() == 1 ){ originalImageLandmarksFound = true; }
                     else{
                            Toast.makeText(getApplicationContext(), R.string.err_No68, Toast.LENGTH_LONG).show();
                        }
                }
                else
                {
                     Toast.makeText(getApplicationContext(), R.string.err_NoTempImage, Toast.LENGTH_LONG).show();
                }


            }

    private void createTempFile() {

        outputImage = MainSingleton.getInstance().getmRGBbitmap();
        photoImage.setImage(ImageSource.bitmap(outputImage));
        bcgView.setBackgroundColor(getResources().getColor(R.color.blackColor,null));

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                FileUtils.saveBitmap(outputImage,Constants.tempName());
                didCreateTemp = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {  showOriginalImage(); } });
            }
        });
    }

    private void showOriginalImage(){

        File file = new File(Constants.appPath()+File.separator + Constants.tempName());
        if (file.exists())
        {
            outputImage = BitmapFactory.decodeFile(file.toString());
            photoImage.setImage(ImageSource.bitmap(outputImage));
        }
    }

    private Bitmap bitmapFromFile(String filename){
       return BitmapFactory.decodeFile(Constants.appPath()+File.separator + filename);
    }
    //</editor-fold>

    //<editor-fold desc="Obsluga TTS">

    private void loadTTSengine() {
        mTts = new TextToSpeech(getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                mTts.setLanguage(new Locale("pl_PL"));

                if (!MainSingleton.getInstance().getDidplayEditTTSIntro()){

                    if (shouldSpeak()){ mTts.speak(getResources().getString(R.string.tts_newFotoInfo), TextToSpeech.QUEUE_ADD, null,"editIntroTTS");}
                    MainSingleton.getInstance().setDidplayEditTTSIntro(true);
                }
            }
        });
    }

    private boolean shouldSpeak(){

        SharedPreferences preferences = getSharedPreferences(MainSingleton.getInstance().getPrefName(), Context.MODE_PRIVATE);
        return preferences.getBoolean("speak", true);
    }

    //</editor-fold>

    //<editor-fold desc="Obsluga przycisków filtrów">

    private void filterButtonPress(final Bitmap landmarkBmp,
                                   final Bitmap filterSkinTexture,
                                   final Enums.FaceShapeBlendType blendType,
                                   final Bitmap accesoryTexture,
                                   final ArrayList<Point> accessoryPoints){

        if (!filter_isActive) {

            if (didCreateTemp && originalImageLandmarksFound){
                averageToAverageManFilter(
                        landmarkBmp,
                        filterSkinTexture,
                        blendType,
                        accesoryTexture,
                        accessoryPoints);

                filter_isActive = true;}
            else{
                Toast.makeText(getApplicationContext(),
                        R.string.err_NoTempImage, Toast.LENGTH_LONG)
                        .show();}
        }
        else{
            showOriginalImage();
            filter_isActive = false;
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideFilterMenu();
            }
        }, buttonDelay);
    }


    public void strongTexturePress(View view){
        if (addNormalTexture){ addNormalTexture = false; }
        else { addNormalTexture = true; }
        updateButtons();
    }

    public void weakTexturePress(View view){
        if (addNormalTexture){ addNormalTexture = false; }
        else { addNormalTexture = true; }
        updateButtons();
    }

    public void debugPress(View view){
        if (debugMode){ debugMode = false; }
        else { debugMode = true; }
        updateButtons();
    }

    public void accesoryPress(View view){
        if (addAbeard){ addAbeard = false; }
        else { addAbeard = true; }
        updateButtons();
    }

    public void nonePress(View view){

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideFilterMenu();
            }
        }, buttonDelay);
    }

    private Enums.FaceShapeBlendType blendType(Enums.FaceShapeBlendType stanadardType, Enums.FaceShapeBlendType strongType){

        Enums.FaceShapeBlendType type;
        if (addNormalTexture){ type = stanadardType;}
        else{ type = strongType;}
        return type;
    }

    public void f1(View view){

        Bitmap accesoryTexture = null;
        ArrayList<Point> accessoryPoints = null;

        if (addAbeard){
            AccesoryTextureData accessoryTexData = new AccesoryTextureData(this);
            accesoryTexture = accessoryTexData.aBeardTexture();
            accessoryPoints = accessoryTexData.aBeardPoints();
        }

        filterButtonPress(

                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_defman),
                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_defman_texture),
                blendType(Enums.FaceShapeBlendType.WhiteMale, Enums.FaceShapeBlendType.WhiteMaleStrong),
                accesoryTexture,
                accessoryPoints
                );
    }

    public void f2(View view){

        Bitmap accesoryTexture = null;
        ArrayList<Point> accessoryPoints = null;

        if (addAbeard){
            AccesoryTextureData accessoryTexData = new AccesoryTextureData(this);
            accesoryTexture = accessoryTexData.aBeardTexture();
            accessoryPoints = accessoryTexData.aBeardPoints();
        }

        filterButtonPress(

                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_defwoman),
                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_defwoman_texture),
                blendType(Enums.FaceShapeBlendType.WhiteFemale, Enums.FaceShapeBlendType.WhiteFemaleStrong),
                accesoryTexture,
                accessoryPoints
        );

    }

    public void f3(View view){
        Bitmap accesoryTexture = null;
        ArrayList<Point> accessoryPoints = null;

        if (addAbeard){
            AccesoryTextureData accessoryTexData = new AccesoryTextureData(this);
            accesoryTexture = accessoryTexData.aBeardTexture();
            accessoryPoints = accessoryTexData.aBeardPoints();
        }

        filterButtonPress(

                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_blackman),
                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_blackman_texture),
                blendType(Enums.FaceShapeBlendType.BlackMale, Enums.FaceShapeBlendType.BlackMaleStrong),
                accesoryTexture,
                accessoryPoints
        );

    }

    public void f4(View view){
        Bitmap accesoryTexture = null;
        ArrayList<Point> accessoryPoints = null;

        if (addAbeard){
            AccesoryTextureData accessoryTexData = new AccesoryTextureData(this);
            accesoryTexture = accessoryTexData.aBeardTexture();
            accessoryPoints = accessoryTexData.aBeardPoints();
        }

        filterButtonPress(

                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_asianman_texture),
                BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.f_asianman_texture),
                blendType(Enums.FaceShapeBlendType.AsianMale, Enums.FaceShapeBlendType.AsianMaleStrong),
                accesoryTexture,
                accessoryPoints
        );

    }

    //</editor-fold>

    //<editor-fold desc="Filtr - Uśrednianie">
    private void averageToAverageManFilter(final Bitmap landmarkBmp,
                                           final Bitmap filterSkinTexture,
                                           final Enums.FaceShapeBlendType blendType,
                                           final Bitmap accesoryTexture,
                                           final ArrayList<Point> accessoryPoints){

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                AverageBitmap avBitmap = new AverageBitmap(
                        bitmapFromFile(Constants.tempName()),
                        filterSkinTexture,
                        MainSingleton.getInstance().getOriginalImageLandmarks(),
                        DlibHelper.landmarksFromBitmap(landmarkBmp),
                        MainSingleton.getInstance().getOriginalImageAvgIllumination(),
                        blendType,
                        debugMode
                );

                final WMJFaceImage faceImage = avBitmap.averageBitmapWithBitmap();
                Bitmap tempBitmap;

                //--- dodajemy akcesoria np. brodę:

                if (accesoryTexture != null && accessoryPoints != null){

                    WMJFaceImage faceImageWithAccessory = avBitmap.faceImageWithAddedAccessories(
                            faceImage,
                            accesoryTexture,
                            accessoryPoints
                            );

                    tempBitmap = faceImageWithAccessory.getBmp();
                }
                else
                {
                    tempBitmap = faceImage.getBmp();
                }
                //----------------------

                final Bitmap resultBitmap = tempBitmap; //gotowa zmieniona bitmapa

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        outputImage = resultBitmap;
                        photoImage.setImage(ImageSource.bitmap(outputImage));

                    } });
            }
        });
    }
    //</editor-fold>




}