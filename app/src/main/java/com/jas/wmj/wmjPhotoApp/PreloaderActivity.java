package com.jas.wmj.wmjPhotoApp;

import android.app.Activity;
import android.os.Bundle;
import android.Manifest;
import android.widget.Toast;
import android.content.Intent;

import permissions.dispatcher.RuntimePermissions;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;

@RuntimePermissions
public class PreloaderActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preloader);

        PreloaderActivityPermissionsDispatcher.continueLoadWithCheck(this);
    }

    @NeedsPermission( { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE })
    public void continueLoad() {

        Intent intent = new Intent(this, CameraCalibrationActivity.class);
        startActivity(intent);
        finish(); //aby zapibiec możliwości powrotu do tego kontrolera
    }

    @OnPermissionDenied( { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE })
    void showDeniedForCamera() {
        Toast.makeText(this, R.string.permission_camera_denied, Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain( { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE })
    void showNeverAskForCamera() {
        Toast.makeText(this, R.string.permission_camera_denied, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Przekazanie zapytań o pozwolenia do wygenerowanej przez PermissionsDispatcher klasy PreloaderActivityPermissionsDispatcher:
         PreloaderActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }
}
