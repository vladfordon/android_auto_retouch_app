package com.jas.wmj.wmjPhotoApp;

import com.jas.wmj.appUtils.DlibHelper;
import com.jas.wmj.appUtils.Enums;
import com.jas.wmj.appUtils.FileUtils;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.core.CvType;

import java.io.File;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.os.Handler;
import android.view.WindowManager;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import java.util.Locale;
import java.util.Random;
import android.widget.TextView;

import com.jas.wmj.appUtils.MainSingleton;
import com.jas.wmj.appUtils.VerticalTextField;
import com.jas.wmj.appUtils.filterHelpers.ImageCorrector;
import com.tzutalin.dlib.Constants;
import com.tzutalin.dlib.PeopleDet;
import com.tzutalin.dlib.VisionDetRet;

import java.util.ArrayList;
import java.util.List;

public class CameraCalibrationActivity extends Activity implements CvCameraViewListener2, SensorEventListener {

    //<editor-fold desc="Zmienne globalne">
    private static final String TAG = "OCVDlib::Activity";

    private RelativeLayout overlayView;
    private View flipBtnLayout;
    private View flipBtnLayoutOverlay;
    private ImageView camIcon;
    private ImageView spinner;
    private ImageView spinnerbl;
    private ImageView imageSoundBtn;
    private VerticalTextField hintTextField_PORT;
    private VerticalTextField hintTextField_UP;
    private TextView hintTextField_LRIGHT;
    private TextView hintTextField_LLEFT;

    private CameraBridgeViewBase mOpenCvCameraView;
    private int cameraIndex = 1;

    private static String landmarkDatamodelPath;
    List<VisionDetRet> landmarkList;

    private boolean isDlibDataModelinLocaton;
    private boolean didSpinnerStopped = false;
    private boolean didOrientationChangeOnce = false;
    private boolean didStatusChange = true;
    private boolean isMakingPhoto = false;
    private boolean isMakingPhotoLocked = false;
    private boolean headIsPositionedInCenter = false;

    //--
    private int frameCount = 0;
    private static int nFramesSkipped = 2;
    private static int frameRescaleFactor = 5;

    private static Scalar lineColor = new Scalar(255, 255, 255);

    private Mat mRgba;
    private Mat mRgbaFull;
    private Bitmap bmp;
    private int wRGBA;
    private int hRGBA;
    private Point centerRGBA;

    private Point lipU;
    private Point lipD;
    private Point lipCornerP;
    private Point lipCornerL;
    private Point lipCornerP_I;
    private Point lipCornerL_I;

    //-- Ladowanie stringów do pamięci dla szybkiego dostępu

    private String getFaceStr;
    private String defaultHintStr;
    private String faceOkHint;

    private String[] tts_noFace;
    private String[] tts_faceOk;
    private String tooManyFaces;
    private String intro_2_str;
    private String tts_centerMove;

    private int ttsQueueCounter = 0;


    private Enums.Status _status = Enums.Status.unknown;
    private Enums.Status _oldStatus = Enums.Status.unknown;

    private TextToSpeech mTts;
    private MediaPlayer mp;

    Random randomGenerator = new Random();
    //</editor-fold>

    //<editor-fold desc="Wykrywanie orientacji urządzenia na podstawie czujników">

    Enums.DeviceOrientation deviceOrientation = Enums.DeviceOrientation.lsleft;
    Enums.DeviceOrientation oldDeviceOrientation = Enums.DeviceOrientation.unknown;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    public int mOrientationDeg; //last rotation in degrees
    private static final int _DATA_X = 0;
    private static final int _DATA_Y = 1;
    private static final int _DATA_Z = 2;

    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    public void onSensorChanged(SensorEvent event) {

        float[] values = event.values;
        int orientation = -1;
        float X = -values[_DATA_X];
        float Y = -values[_DATA_Y];
        float Z = -values[_DATA_Z];
        float magnitude = X * X + Y * Y;
        // Zignoruj kont jeżeli wartość magnitude (wielkość) jest zbyt mała w stosunku do wartości y
        if (magnitude * 4 >= Z * Z) {
            float OneEightyOverPi = 57.29577957855f;
            float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
            orientation = 90 - (int) Math.round(angle);
            // normalizacja do zasięgu 0 - 359 stopni
            while (orientation >= 360) {
                orientation -= 360;
            }
            while (orientation < 0) {
                orientation += 360;
            }
        }
        //ustalenie aktualnej orientacji urządzenia:

        if (orientation != mOrientationDeg) {

            mOrientationDeg = orientation;
            //figure out actual orientation
            if (orientation != -1) {//basically flat

                if (orientation <= 45 || orientation > 315) {// 0
                    deviceOrientation = Enums.DeviceOrientation.portrait;

                } else if (orientation > 45 && orientation <= 135) {// 90
                    deviceOrientation = Enums.DeviceOrientation.lsleft;

                } else if (orientation > 135 && orientation <= 225) {// 180
                    deviceOrientation = Enums.DeviceOrientation.upsidedown;

                } else if (orientation > 225 && orientation <= 315) {// 270
                    deviceOrientation = Enums.DeviceOrientation.lsright;
                }

                updateOverlayWithOrientation(deviceOrientation);
            }
        }
    }

    private boolean orientationDidChange() {
        boolean q = didOrientationChangeOnce;

        if (oldDeviceOrientation == deviceOrientation) {
            q = false;
        }
        oldDeviceOrientation = deviceOrientation;
        didOrientationChangeOnce = true;

        return q;
    }
    //</editor-fold>

    //<editor-fold desc="OpenCVLoader">
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    //---
    private void loadOpenCVWithCameraIndex(int index) {
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.activity_camera_surface);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCameraIndex(index);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.mScale = 1.225f;
    }
    //</editor-fold>

    //<editor-fold desc="OpenCV -> Implementacja delegata">
    public void onCameraViewStarted(int width, int height) {

        mRgba = new Mat(height, width, CvType.CV_8UC4);
        if (wRGBA == 0){ wRGBA = mRgba.cols(); }
        if (hRGBA == 0 ){ hRGBA = mRgba.rows(); }
        if (centerRGBA == null){ centerRGBA = new Point( wRGBA / 2, hRGBA / 2); }
    }

    public void onCameraViewStopped() {
        mRgba.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();
        Mat tempMat = inputFrame.gray();

        if (cameraIndex == 1) {

            Core.flip(mRgba, mRgba, 1); //Core.flip --> Jest dużo szybsze od Imgproc.warpAffine --> ~10 klatek/s różnicy
            if (deviceOrientation == Enums.DeviceOrientation.lsright) { Core.flip(tempMat, tempMat, 1); } //flip X
        }

        if (!isMakingPhoto) {
            mRgbaFull = mRgba.clone();

            //-------------------------------- dlib:
            if (isDlibDataModelinLocaton) {

                if (frameCount % nFramesSkipped == 0) {

                    Size tDestSize = new Size(tempMat.width() / frameRescaleFactor, tempMat.height() / frameRescaleFactor);
                    Imgproc.resize(tempMat, tempMat, tDestSize);
                    if (cameraIndex == 1) {

                        switch (deviceOrientation) {
                            case portrait:

                                Core.transpose(tempMat, tempMat);
                                Core.flip(tempMat, tempMat, 0); //flipX
                                break;

                            case upsidedown:

                                Core.transpose(tempMat, tempMat);
                                Core.flip(tempMat, tempMat, 1); //flipY
                                break;

                            case lsleft:
                                Core.flip(tempMat, tempMat, 0); //flipX
                                break;

                            default:
                                break;
                        }
                    } else if (cameraIndex == 0) {
                        switch (deviceOrientation) {
                            case portrait:

                                Core.transpose(tempMat, tempMat);
                                break;

                            case upsidedown:

                                Core.transpose(tempMat, tempMat);
                                Core.flip(tempMat, tempMat, -1);  //flipXY
                                break;

                            case lsleft:

                                Core.flip(tempMat, tempMat, -1); //flipXY
                                break;

                            default:
                                break;
                        }
                    }

                    bmp = Bitmap.createBitmap(tempMat.cols(), tempMat.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(tempMat, bmp);

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                             if (!isMakingPhoto){ //Blokuje wykonanie operacji zakolejkowanej asynchronicznie, gdy zdjęcie jest wykonywane -> przyspiesza pierwsze ładowanie ekranu edycji zdjęcia
                                landmarkList = MainSingleton.getInstance().getLandmarkDetector().detBitmapFace(bmp, landmarkDatamodelPath);
                            }
                        }
                    });

                }

                if (landmarkList != null) {

                    if (!didSpinnerStopped) {
                        didSpinnerStopped = true;
                        ((AnimationDrawable) spinner.getBackground()).stop();
                        if (spinner.getAlpha() > 0f) {
                            spinner.setAlpha(0f);
                        }
                    }


                    if (landmarkList.size() == 0) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                _status = Enums.Status.noFace;
                                setHintText(getFaceStr);
                            }
                        });
                    } else if (landmarkList.size() == 1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                _status = Enums.Status.faceTimeNoSmile;
                                setHintText(defaultHintStr);

                                //=================================

                                if (lipD.x != 0) {

                                    boolean smileOk = false;
                                    switch (deviceOrientation) {
                                        case portrait:

                                            if (lipU.x < lipD.x
                                                    || (lipCornerP.x < lipCornerP_I.x || lipCornerL.x < lipCornerL_I.x)
                                                    || (lipCornerP.x <= lipD.x && lipCornerL.x <= lipD.x)
                                                    ) {
                                                smileOk = true;
                                            }
                                            break;

                                        case upsidedown:

                                            if (lipU.x > lipD.x) {
                                                smileOk = true;
                                            }
                                            break;

                                        case lsleft:

                                            if (lipU.y > lipD.y) {
                                                smileOk = true;
                                            }
                                            break;

                                        case lsright:

                                            if (lipU.y < lipD.y
                                                    || (lipCornerP.y < lipCornerP_I.y && lipCornerL.y < lipCornerL_I.y)
                                                    || (lipCornerP.y <= lipD.y && lipCornerL.y <= lipD.y)
                                                    ) {
                                                smileOk = true;
                                            }
                                            break;

                                        default:
                                            break;
                                    }


                                    if (smileOk) {
                                        _status = Enums.Status.faceTimeOk;
                                        setHintText(faceOkHint);
                                    }


                                }

                                //==== detekcja uśmiechu

                            }
                        });
                    } else {
                        _status = Enums.Status.tooManyFaces;
                    }


                    int i = 0;
                    for (final VisionDetRet ret : landmarkList) {

                        // Rysowanie landmarków

                        ArrayList<android.graphics.Point> landmarks = ret.getFaceLandmarks();
                        for (android.graphics.Point point : landmarks) {

                            int pointX = 0;
                            int pointY = 0;

                            if (deviceOrientation == Enums.DeviceOrientation.lsright) {

                                pointX = (point.x * frameRescaleFactor);
                                pointY = (point.y * frameRescaleFactor);

                            } else if (deviceOrientation == Enums.DeviceOrientation.lsleft) {

                                pointX = wRGBA - (point.x * frameRescaleFactor);
                                pointY = hRGBA - (point.y * frameRescaleFactor);

                            } else if (deviceOrientation == Enums.DeviceOrientation.portrait) {

                                pointX = (point.y * frameRescaleFactor);
                                pointY = (point.x * frameRescaleFactor);

                            } else if (deviceOrientation == Enums.DeviceOrientation.upsidedown) {

                                pointX = wRGBA - (point.y * frameRescaleFactor);
                                pointY = hRGBA - (point.x * frameRescaleFactor);
                            }


                            if (i == 62) {
                                lipU.x = pointX;
                                lipU.y = pointY;

                            } else if (i == 66) {
                                lipD.x = pointX;
                                lipD.y = pointY;

                            } else if (i == 48) {
                                lipCornerL.x = pointX;
                                lipCornerL.y = pointY;
                            } else if (i == 54) {
                                lipCornerP.x = pointX;
                                lipCornerP.y = pointY;
                            } else if (i == 60) {
                                lipCornerL.x = pointX;
                                lipCornerL.y = pointY;
                            } else if (i == 64) {
                                lipCornerP.x = pointX;
                                lipCornerP.y = pointY;
                            }

                            Imgproc.circle(mRgba, new Point(pointX, pointY), 2, lineColor);
                            i++;
                        }

                    }

                }
            }

            frameCount++;
            if (frameCount > 4) {
                playVoice(); // odezwa głosowa co 4 klatek
                frameCount = 0;
            }
        }
        return mRgba;
    }
    //========================================================================
    //</editor-fold>

    //<editor-fold desc="Obsługa interfejsu użytkownika">

    private void startTakingPhoto(){

        int resID = R.raw.warning; // sygnał dzwiękowy -> wykonanie zdjęcia nie jest możliwe
        boolean shouldSpeak = shouldSpeak();
        boolean shouldTakePhoto = shouldTakePhoto();


        if (shouldTakePhoto && !isMakingPhotoLocked) {


            resID = R.raw.polaroid_camera;
        }

        //odpowiedni komunikat dzwiekowy, kiedy dzwięk jest włączony
        if (!(!shouldSpeak && shouldTakePhoto)) {

            mp = MediaPlayer.create(CameraCalibrationActivity.this, resID);
            if (mp != null) {

                if (!mp.isPlaying()) {
                    mp.start();
                }
            }
        }

        if (!headIsPositionedInCenter
                && shouldSpeak
                && (_status == Enums.Status.faceTimeOk)||_status == Enums.Status.faceTimeNoSmile) {

            if (mTts.isSpeaking()){mTts.stop();}
            mTts.speak(tts_centerMove, TextToSpeech.QUEUE_ADD, null,null);
        }
    }

    private void takePhotoAction() {
        if (shouldTakePhoto() && !isMakingPhotoLocked) {

            isMakingPhoto = true;
            isMakingPhotoLocked = true;
			startSpinnerBlack();

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    if (mOpenCvCameraView != null) {
                        mOpenCvCameraView.disableView();
                        mOpenCvCameraView = null;
                    }
                }
            });

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    if (mTts != null) {
                        mTts.stop();
                    }
                    mSensorManager.unregisterListener(CameraCalibrationActivity.this);
                }
            });

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    if (cameraIndex == 1) {

                        switch (deviceOrientation) {
                            case portrait:

                                Core.transpose(mRgbaFull, mRgbaFull);
                                Core.flip(mRgbaFull, mRgbaFull, 1); //flipY
                                break;

                            case upsidedown:

                                Core.transpose(mRgbaFull, mRgbaFull);
                                Core.flip(mRgbaFull, mRgbaFull, 0); //flipX
                                break;

                            case lsleft:
                                Core.flip(mRgbaFull, mRgbaFull, -1); //flipXY
                                break;

                            default:
                                break;
                        }
                    } else if (cameraIndex == 0) {
                        switch (deviceOrientation) {
                            case portrait:

                                Core.transpose(mRgbaFull, mRgbaFull);
                                Core.flip(mRgbaFull, mRgbaFull, 1); //flipY
                                break;

                            case upsidedown:

                                Core.transpose(mRgbaFull, mRgbaFull);
                                Core.flip(mRgbaFull, mRgbaFull, 0); //flipX
                                break;

                            case lsleft:

                                Core.flip(mRgbaFull, mRgbaFull, -1); //flipXY
                                break;

                            default:
                                break;
                        }
                    }

                    //Odczyt średniej iluminacji zdjęcia:
                    ImageCorrector imCor = new ImageCorrector();
                    MainSingleton.getInstance().setOriginalImageAvgIllumination(imCor.averageIluninationFromMat(mRgbaFull));

                    //------------------------------
                    // Tworzenie właściwego zdjęcia:
                    Bitmap bmpTmpRGBA = Bitmap.createBitmap(mRgbaFull.cols(), mRgbaFull.rows(), Bitmap.Config.RGB_565);

                    Utils.matToBitmap(mRgbaFull, bmpTmpRGBA);
                    MainSingleton.getInstance().setmRGBbitmap(bmpTmpRGBA);
                    //---------------------------------

                    List<VisionDetRet> list = originalImageLandmarkListForEditActivity();
                    if (list!=null) {

                        MainSingleton.getInstance().setOriginalImageLandmarks(list);

                        final File file = new File(Constants.appPath() + File.separator, Constants.tempName());

                        if (file.exists()) {
                            file.delete();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Intent intent = new Intent(CameraCalibrationActivity.this, PhotoEditActivity.class);
                                stopSpinnerBlack();

                                isMakingPhoto = false;
                                startActivity(intent);
                                finish();
                            }
                        });
                    }
                    else
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (mp!=null){
                                    if (mp.isPlaying()){mp.stop();}
                                    mp = MediaPlayer.create(CameraCalibrationActivity.this,R.raw.warning);
                                    mp.start();
                                }

                                stopSpinnerBlack();
                                finish();
                                // w przypadku krytycznego błędu (brak koniecznej ilości wykrytch elementów twarzy) klasa jest ładowana ponownie
                                startActivity(getIntent());

                            }});

                    }
                }
            });
        }
    }

    private void stopSpinnerBlack(){

        overlayView.setBackgroundColor(getResources().getColor(R.color.transparentColor, null));
        ((AnimationDrawable) spinnerbl.getBackground()).stop();

        if (flipBtnLayout.getVisibility() !=  View.VISIBLE){ flipBtnLayout.setVisibility(View.VISIBLE); }
        if (flipBtnLayoutOverlay.getVisibility() !=  View.VISIBLE){ flipBtnLayoutOverlay.setVisibility(View.VISIBLE); }
        spinnerbl.setAlpha(0.0f);

    }

    private void startSpinnerBlack(){

        overlayView.setBackgroundColor(getResources().getColor(R.color.whiteColor, null));
        overlayView.setClickable(false);
        //if (flipBtnLayout.getVisibility() == View.VISIBLE) {
            flipBtnLayout.setVisibility(View.INVISIBLE);
       // }
        //if (flipBtnLayoutOverlay.getVisibility() == View.VISIBLE) {
            flipBtnLayoutOverlay.setVisibility(View.INVISIBLE);
       // }
        //if (mOpenCvCameraView.getVisibility() == View.VISIBLE) {
            mOpenCvCameraView.setVisibility(View.INVISIBLE);
       // }

        spinnerbl.setAlpha(1.0f);
        ((AnimationDrawable) spinnerbl.getBackground()).start();
    }
	
    private List<VisionDetRet> originalImageLandmarkListForEditActivity(){

        //pozyskanie landmarków z obrazu przeznaczonego do przysłania do ekranu edycji
        // czynnik precyzji jest 2x wyższy niż w przypadku landmarków wyświetlanych podczas kalibracji

        List<VisionDetRet> result;

        int precossionFactor = frameRescaleFactor / 2;

        Mat sourceTmp = new Mat (mRgbaFull.width(),mRgbaFull.height(),CvType.CV_8UC1);
        Imgproc.cvtColor(mRgbaFull, sourceTmp, Imgproc.COLOR_RGB2GRAY);

        Size tDestSize = new Size(mRgbaFull.width() / precossionFactor, mRgbaFull.height() / precossionFactor);
        Imgproc.resize(sourceTmp, sourceTmp, tDestSize);

        Bitmap grayBitmap = Bitmap.createBitmap(mRgbaFull.cols()/precossionFactor, mRgbaFull.rows()/precossionFactor, Bitmap.Config.RGB_565);
        Utils.matToBitmap(sourceTmp, grayBitmap);

        result = MainSingleton.getInstance().getLandmarkDetector().detBitmapFace(grayBitmap, landmarkDatamodelPath);

        if (!result.isEmpty()) {
            VisionDetRet ret = result.get(0);
            ArrayList<android.graphics.Point> landmarks = ret.getFaceLandmarks();

            int i = 0;
            for (android.graphics.Point point : landmarks) {

                landmarks.set(i, new android.graphics.Point(point.x * precossionFactor, point.y * precossionFactor));
                i++;
            }

            ret.setFaceLandmarks(landmarks);
            result.set(0, ret);
        }
        else {
            result = null;
        }

        return result;
    }

    private boolean shouldTakePhoto(){

        boolean anwser = false;
        headIsPositionedInCenter = false;

        if ((_status == Enums.Status.faceTimeOk || _status == Enums.Status.faceTimeNoSmile) && mRgbaFull != null && landmarkList != null){

            if (landmarkList.size() == 1){

                ArrayList<android.graphics.Point> landmarks = DlibHelper.listOfPointsFromLandmarks(landmarkList);
                if (landmarks.size() == 68){

                    double marginY = 0.05;
                    double marginX = 0.05;
                    if (deviceOrientation == Enums.DeviceOrientation.lsright || deviceOrientation == Enums.DeviceOrientation.lsleft){
                        marginY = 0.2;
                        marginX = 0.15;
                    }

                    int minX = (int)((double)bmp.getWidth() * marginX);
                    int minY = (int)((double)bmp.getHeight() * marginY);

                    int maxX = bmp.getWidth() - minX;
                    int maxY = bmp.getHeight() - minY;

                    anwser = true;
                    headIsPositionedInCenter = true;

                    for (android.graphics.Point point : landmarks){

                        if (point.x <= minX || point.y <= minY || point.x >= maxX || point.y >= maxY ){

                            headIsPositionedInCenter = false;
                            return false;
                        }
                    }

                } //zdjęcie wykonywane jest tylko gdy odnaleziono 68 punków i punkty te znajdują się w obaszarze poza krawędziami obrazu

            }
        }
        return anwser;
    }

    public void soundBtnAction(View v){

        if (mTts != null){
            if (mTts.isSpeaking()){mTts.stop();}
        }

        SharedPreferences preferences = getSharedPreferences(MainSingleton.getInstance().getPrefName(), Context.MODE_PRIVATE);
        boolean speak = preferences.getBoolean("speak", true);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("speak", !speak);
        editor.commit();

        if (speak){ imageSoundBtn.setImageResource(R.mipmap.soundoff); }
        else { imageSoundBtn.setImageResource(R.mipmap.soundon); }
    }

    public void cameraFlipAction(View v) {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Opóźnienie 50ms

                overlayView.setBackgroundColor(getResources().getColor(R.color.blackColor,null));
                mOpenCvCameraView.disableView();

                if (cameraIndex == 1) { cameraIndex = 0; }
                else { cameraIndex = 1; }

                mOpenCvCameraView.setCameraIndex(cameraIndex);
                mOpenCvCameraView.enableView();

                overlayView.setBackgroundColor(getResources().getColor(R.color.transparentColor,null));
                overlayView.setBackground(getResources().getDrawable(R.drawable.ripple_effect,null));
            }
        }, 50);
    }

    private void updateOverlayWithOrientation(Enums.DeviceOrientation orientation) {
        if (orientationDidChange() && didOrientationChangeOnce) { //pozycjonowanie interfejsu użytkownika

            switch (orientation) {
                case portrait:

                    camIcon.setRotation(-90);
                    imageSoundBtn.setRotation(-90);


                    hintTextField_LLEFT.setAlpha(0f);
                    hintTextField_LRIGHT.setAlpha(0f);
                    hintTextField_PORT.setAlpha(1f);
                    hintTextField_UP.setAlpha(0f);

                    break;
                case upsidedown:

                    camIcon.setRotation(90);
                    imageSoundBtn.setRotation(90);

                    hintTextField_LLEFT.setAlpha(0f);
                    hintTextField_LRIGHT.setAlpha(0f);
                    hintTextField_PORT.setAlpha(0f);
                    hintTextField_UP.setAlpha(1f);

                    break;
                case lsleft:

                    camIcon.setRotation(180);
                    imageSoundBtn.setRotation(180);

                    hintTextField_LLEFT.setAlpha(1f);
                    hintTextField_LRIGHT.setAlpha(0f);
                    hintTextField_PORT.setAlpha(0f);
                    hintTextField_UP.setAlpha(0f);

                    break;

                case lsright:

                    camIcon.setRotation(0);
                    imageSoundBtn.setRotation(0);

                    hintTextField_LLEFT.setAlpha(0f);
                    hintTextField_LRIGHT.setAlpha(1f);
                    hintTextField_PORT.setAlpha(0f);
                    hintTextField_UP.setAlpha(0f);

                    break;

                default:

                    break;
            }

            if (flipBtnLayout.getVisibility() !=  View.VISIBLE){ flipBtnLayout.setVisibility(View.VISIBLE); }
            if (flipBtnLayoutOverlay.getVisibility() !=  View.VISIBLE){ flipBtnLayoutOverlay.setVisibility(View.VISIBLE); }
        }
    }

    private void loadUI(){
        overlayView = (RelativeLayout) findViewById(R.id.cameraOverlay);

        flipBtnLayout = findViewById(R.id.flipBtnX);
        flipBtnLayoutOverlay = findViewById(R.id.flipBtnXOverlay);

        flipBtnLayout.setVisibility(View.VISIBLE);
        flipBtnLayoutOverlay.setVisibility(View.VISIBLE);

        camIcon = (ImageView) findViewById(R.id.flipBtnArrowCamImg);
        spinner = (ImageView) findViewById(R.id.spinner);
        spinnerbl = (ImageView) findViewById(R.id.spinnerbl);
        spinnerbl.setAlpha(0.0f);

        imageSoundBtn = (ImageView) findViewById(R.id.imageSoundBtn);

        hintTextField_LLEFT = (TextView) findViewById(R.id.hintTextField_LLEFT);
        hintTextField_LRIGHT = (TextView) findViewById(R.id.hintTextField_LRIGHT);
        hintTextField_PORT = (VerticalTextField) findViewById(R.id.hintTextField_PORT);
        hintTextField_UP = (VerticalTextField) findViewById(R.id.hintTextField_UPSIDE);

        intro_2_str = getResources().getString(R.string.tts_intro_end);
        setHintText(getResources().getString(R.string.wait_str));
        _status = Enums.Status.waiting;

        getFaceStr = getResources().getString(R.string.get_face);
        defaultHintStr = getResources().getString(R.string.def_hint_text);
        faceOkHint = getResources().getString(R.string.smile_str);
        tts_centerMove = getResources().getString(R.string.tts_centerMove);

        tts_faceOk = new String[]{

                getResources().getString(R.string.tts_faceOk_0),
                getResources().getString(R.string.tts_faceOk_1),
                getResources().getString(R.string.tts_faceOk_2),
                getResources().getString(R.string.tts_faceOk_3),
                getResources().getString(R.string.tts_faceOk_4),
                getResources().getString(R.string.tts_faceOk_5),
                getResources().getString(R.string.tts_faceOk_6),
                getResources().getString(R.string.tts_faceOk_7)
        };

        tts_noFace = new String[]{

                getResources().getString(R.string.tts_noFace_0),
                getResources().getString(R.string.tts_noFace_1),
                getResources().getString(R.string.tts_noFace_2),
                getResources().getString(R.string.tts_noFace_3),
                getResources().getString(R.string.tts_noFace_4),
        };

        tooManyFaces = getResources().getString(R.string.tts_oneTooMantyFaces);

        ((AnimationDrawable) spinner.getBackground()).start();

        overlayView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if (mTts.isSpeaking()){mTts.stop();}

                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startTakingPhoto();
                        break;
                    case MotionEvent.ACTION_UP:
                        takePhotoAction();
                        break;
                }
                return false;


            }
        });

        if (shouldSpeak()){ imageSoundBtn.setImageResource(R.mipmap.soundon); }
        else{ imageSoundBtn.setImageResource(R.mipmap.soundoff); }
    }

    private void setHintText(String text){

        hintTextField_UP.setText(text);
        hintTextField_PORT.setText(text);
        hintTextField_LRIGHT.setText(text);
        hintTextField_LLEFT.setText(text);
    }
    //</editor-fold>

    //<editor-fold desc="dlib Loader">
    private void loadDlib() {

        isDlibDataModelinLocaton = true;
        landmarkDatamodelPath = Constants.getFaceShapeModelPath();

        lipD = new Point(0,0);
        lipU = new Point(0,0);
        lipCornerP = new Point(0,0);
        lipCornerL = new Point(0,0);
        lipCornerP_I = new Point(0,0);
        lipCornerL_I = new Point(0,0);
        //Powyżej: wybrane punkty - dla detekcji uśmiechu

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                if (!new File(landmarkDatamodelPath).exists()) {

                    File f = new File(Constants.appPath());
                    f.mkdirs();

                    isDlibDataModelinLocaton = false;
                    FileUtils.copyFileFromRawToOthers(CameraCalibrationActivity.this, R.raw.shape_predictor_68_face_landmarks, landmarkDatamodelPath);
                    isDlibDataModelinLocaton = true;
                }

            }
        });

        MainSingleton.getInstance().setLandmarkDetector(new PeopleDet());

        // landmarkDetector = new PeopleDet();
    }
    //</editor-fold>

    //<editor-fold desc="Cykl życia klasy">
    public CameraCalibrationActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // createDummyTemp();
        loadTTSengine();
        loadDlib();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        loadUI();
        loadOpenCVWithCameraIndex(cameraIndex);

        //-----------
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //-----------
    }

    @Override
    public void onStart() {
        super.onStart();

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {

            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        updateOverlayWithOrientation(deviceOrientation);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mTts != null){mTts.stop();}

        mSensorManager.unregisterListener(this);
        if (mOpenCvCameraView != null){ mOpenCvCameraView.disableView(); }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mTts != null){mTts.stop();}

        mSensorManager.unregisterListener(this);
        if (mOpenCvCameraView != null){ mOpenCvCameraView.disableView(); }
    }

    public void onDestroy() {
        super.onDestroy();

        if (mTts != null){mTts.shutdown();}

        mSensorManager.unregisterListener(this);
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    //========================================================================
    //</editor-fold>

    //<editor-fold desc="Obsługa TTS">
    private void loadTTSengine() {

        mTts = new TextToSpeech(getBaseContext(), new OnInitListener() {
            @Override
            public void onInit(int status) {
                ttsInit();

                if (!MainSingleton.getInstance().getDidPlayIntroTTS() && _status == Enums.Status.waiting){

                    if (shouldSpeak()){ mTts.speak(getResources().getString(R.string.tts_intro), TextToSpeech.QUEUE_ADD, null,"introTTS");}

                    ttsQueueCounter++;
                    MainSingleton.getInstance().setDidPlayIntroTTS(true);
                }
            }
        });
    }

    private boolean shouldSpeak(){

        SharedPreferences preferences = getSharedPreferences(MainSingleton.getInstance().getPrefName(), Context.MODE_PRIVATE);
        return preferences.getBoolean("speak", true);
    }

    private void playVoice() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                if (statusDidChange()
                    //&& !mTts.isSpeaking()
                        )
                {
                    String txt;
                    if (_status == Enums.Status.noFace) { txt = tts_noFace[randomGenerator.nextInt(5)]; }
                    else if(_status == Enums.Status.tooManyFaces) { txt = tooManyFaces; }
                    else  { txt = tts_faceOk[randomGenerator.nextInt(8)]; }

                    if (shouldSpeak()){ mTts.speak(txt, TextToSpeech.QUEUE_ADD, null, "blurbID"); }

                    ttsQueueCounter++;
                }
                else if (_status == Enums.Status.unknown)
                {
                    if (shouldSpeak()){ mTts.speak(intro_2_str, TextToSpeech.QUEUE_ADD, null,"introID"); }
                    ttsQueueCounter++;
                }

            }
        });

    }
    //================= Delegat Syntezatora TTS

    private void ttsInit(){

        mTts.setLanguage(new Locale("pl_PL"));
        mTts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {}

            @Override
            public void onError(String utteranceId) {}

            @Override
            public void onDone(String utteranceId) {

                if (mTts != null){

                    if (ttsQueueCounter > 3)
                        mTts.stop();
                    ttsQueueCounter = 0;

                }
            }
        });
    }

    private boolean statusDidChange() {
        boolean q = didStatusChange;

        if (_oldStatus == _status) {
            q = false;
        }
        else if ((_oldStatus == Enums.Status.faceTimeOk && _status == Enums.Status.faceTimeNoSmile) ||
                (_status == Enums.Status.faceTimeOk && _oldStatus == Enums.Status.faceTimeNoSmile))
        {
            q = false;
        }

        _oldStatus = _status;
        didStatusChange = true;

        return q;
    }

    //</editor-fold>
}