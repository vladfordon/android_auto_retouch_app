package com.jas.wmj.appUtils.filterHelpers;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

import static java.lang.Math.abs;
import static org.opencv.core.Core.add;
import static org.opencv.core.Core.divide;
import static org.opencv.core.Core.multiply;
import static org.opencv.core.Core.subtract;
import static org.opencv.core.CvType.CV_32FC3;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2BGRA;
import static org.opencv.imgproc.Imgproc.COLOR_BGRA2BGR;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.rectangle;

/**
 * Created by WMJ on 2017-05-30.
 */

public class WMJImageLayerGenerator {

    public Mat filerCanvas(int ax, int by, double scaleFactorToOriginal, int type, Bitmap filterBmp, Size originalImageSize){

        //przeskalowanie bitmapy filtra
        filterBmp = Bitmap.createScaledBitmap(filterBmp, (int)((double)filterBmp.getWidth() * scaleFactorToOriginal), (int)((double)filterBmp.getHeight() * scaleFactorToOriginal), false);

        //konwersja tekstury filtra do postaci macierzy OpenCV
        Mat filterMat = new Mat((int)((double)filterBmp.getHeight() * scaleFactorToOriginal), (int)((double)filterBmp.getWidth() * scaleFactorToOriginal),  type);
        Utils.bitmapToMat(filterBmp,filterMat);

        //tworzenie ramki oraz umieszczenie na niej odpowiednio przeskalowaniej tekstury twarzy
        Mat filterCanvasMat = new Mat(originalImageSize, type, this.cornerPixelColorFromMat(filterMat));
        Rect filterFrame = new Rect(ax, by, filterMat.cols(), filterMat.rows());


        /*
        Log.d("xxx", "ax: "+ax);
        Log.d("xxx", "by: "+by);

        Log.d("xxx", "filterMat.cols(): "+filterMat.cols());
        Log.d("xxx", "filterMat.rows(): "+filterMat.rows());
        Log.d("xxx", "filterMat.type(): "+filterMat.type());

        Log.d("xxx", "filterCanvasMat.cols(): "+filterCanvasMat.cols());
        Log.d("xxx", "filterCanvasMat.rows(): "+filterCanvasMat.rows());
        Log.d("xxx", "filterCanvasMat.type(): "+filterCanvasMat.type());

        Log.d("xxx", "filterFrame: w: "+filterFrame.width +" h: "+ filterFrame.height+" x: "+filterFrame.x + " y: "+filterFrame.y );



            /*
        if (filterMat.cols() >= filterCanvasMat.cols() && filterMat.rows() >= filterCanvasMat.rows()){

            double wDiff = abs(filterMat.cols() - filterCanvasMat.cols());
            double hDiff = abs(filterMat.rows() - filterCanvasMat.rows());
            Rect filterFrameX = new Rect(
                    (int)(wDiff * 0.5),
                    (int)(hDiff * 0.5),
                    filterCanvasMat.cols(),
                    filterCanvasMat.rows()
            );

            filterMat.submat(filterFrameX).copyTo( filterCanvasMat );

        }
        else{ filterMat.copyTo( filterCanvasMat.submat( filterFrame ) );}
        */

        //rectangle( filterCanvasMat, new Point(filterFrame.x, filterFrame.y), new Point(filterFrame.x + filterFrame.width, filterFrame.y + filterFrame.height), new Scalar(0.0,255.0,0.0,255.0), 3);

        filterMat.copyTo( filterCanvasMat.submat( filterFrame ));
        return filterCanvasMat;
    }

    public Scalar cornerPixelColorFromMat(Mat mat){
        double [] filterbcgColor = mat.get(1,1);
        return new Scalar(filterbcgColor[0], filterbcgColor[1], filterbcgColor[2], filterbcgColor[3]);
    }


    public Mat imageWithRGBAoverlay(Mat img, Mat overlay, double blendFactor, Rect rect, double colorTresholdForBlend ){

        Mat tempMat = new Mat(img.size(),img.type());
        double blendScale = 1.0 / 255.0;

        int colMin = rect.x;
        if (colMin < 1){colMin = 1;}

        int rowMin = rect.y;
        if (rowMin < 1){rowMin = 1;}

        int colMax = rect.x + rect.width;
        if (colMax > img.cols()){ colMax = img.cols(); }

        int rowMax = rect.y + rect.height;
        if (rowMax > img.rows()){ rowMax = img.rows(); }


        for (int row = rowMin ; row < rowMax ; row++) {
            for (int column = colMin ; column < colMax ; column++) {

                double[] originalPixel = img.get(row, column);
                double[] overlayPixel = overlay.get(row, column);

                if (overlayPixel[3] > 0.0){

                    double blend = blendFactor * overlayPixel[3] * blendScale;

                    double[] newPixel = {
                            colorValueWithBlend(originalPixel[0], overlayPixel[0], blend, colorTresholdForBlend),
                            colorValueWithBlend(originalPixel[1], overlayPixel[1], blend, colorTresholdForBlend),
                            colorValueWithBlend(originalPixel[2], overlayPixel[2], blend, colorTresholdForBlend),
                            originalPixel[3]
                    };

                    tempMat.put(row, column, newPixel);
                }
                else{
                    tempMat.put(row, column, originalPixel);
                }

            }
        }

        Mat output = new Mat(img.size(),img.type());
        img.copyTo(output);

        tempMat.submat(rect).copyTo( output.submat(rect) );
        return output;
    }

    private double colorValueWithBlend(double firstVal, double secondVal, double blendFactor, double colorTresholdForBlend ){

        if (firstVal < colorTresholdForBlend){

            return secondVal;
        }

        double blendFactorOpposite = 1.0 - blendFactor;
        return firstVal * blendFactorOpposite + secondVal * blendFactor;
    }









    /*
    //--------------- Algorytm mieszania Mat(BGR/BGRA) zgodnie z kanałem alfa wg. https://www.learnopencv.com/alpha-blending-using-opencv-cpp-python/

public Mat blendWithMask(Mat foreground, Mat background, Mat alpha){

    Mat _foreground = new Mat();
    cvtColor(foreground,_foreground, COLOR_BGRA2BGR);

    Mat _background = new Mat();
    cvtColor(background, _background, COLOR_BGRA2BGR);

    Mat _alpha = new Mat();
    cvtColor(alpha, _alpha, COLOR_BGRA2BGR);

    // Convert Mat to float data type
    _foreground.convertTo(_foreground, CV_32FC3);
    _background.convertTo(_background, CV_32FC3);

    // Normalize the alpha mask to keep intensity between 0 and 1
    _alpha.convertTo(_alpha, CV_32FC3, 1.0/255); //

    // Storage for output image
    Mat ouImage = Mat.zeros(_foreground.size(), _foreground.type());

    Log.d("xxx","_foreground: "+_foreground.width() + " " + _foreground.height() + " " + _foreground.type());
    Log.d("xxx","_background: "+_background.width() + " " + _background.height() + " " + _background.type());
    Log.d("xxx","ouImage: "+ouImage.width() + " " + ouImage.height() + " " + ouImage.type());

    // Multiply the foreground with the alpha matte

    Log.d("xxx","zzzz-3");
    multiply(_alpha, _foreground, _foreground);


    // Multiply the background with ( 1 - alpha )

    //Met, Scalar, Mat out

    Mat subtr = new Mat();

    Log.d("xxx","zzzz-2");
    subtract(Mat.ones(_alpha.size(),_alpha.type()),_alpha,subtr);

    Log.d("xxx","zzzz-1");
    multiply(subtr, _background , _background);

    Log.d("xxx","zzzz0");

    add(_foreground, _background, ouImage);

    Log.d("xxx","zzzz1");

    divide(ouImage, new Scalar(255.0),ouImage);

    Log.d("xxx","zzzz2");

    cvtColor(_background, _background, COLOR_BGR2BGRA);

    Log.d("xxx","zzzz3");

    return ouImage;
}

*/
}
