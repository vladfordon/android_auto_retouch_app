package com.jas.wmj.appUtils.filterHelpers;

import android.graphics.Point;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.addWeighted;
import static org.opencv.imgproc.Imgproc.COLOR_BGRA2GRAY;
import static org.opencv.imgproc.Imgproc.COLOR_GRAY2BGRA;
import static org.opencv.imgproc.Imgproc.THRESH_TOZERO;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.fillPoly;

/**
 * Created by WMJ on 2017-05-16.
 */

public class WMJmask {

    public static Mat polyMask(int[] maskPointIndexes, ArrayList<Point> pointsAvg, Size size){

        //CV_8UC1
        Mat polyMask = Mat.zeros(size, CvType.CV_8UC1);

        ArrayList<android.graphics.Point> polyMaskPoints = new ArrayList<>();
        for (int index : maskPointIndexes){ polyMaskPoints.add( pointsAvg.get(index) ); }

        List<MatOfPoint> polyMaskPointsList = new ArrayList<>();

        polyMaskPointsList.add( Convert.listOfPointsToMatOfPoints( polyMaskPoints ));
        fillPoly(polyMask, polyMaskPointsList , new Scalar(255,255,255,1));

        return polyMask;
    }

    public static Mat polyMask( ArrayList<android.graphics.Point> polyMaskPoints, Size size){

        //CV_8UC1
        Mat polyMask = Mat.zeros(size, CvType.CV_8UC1);

        List<MatOfPoint> polyMaskPointsList = new ArrayList<>();

        polyMaskPointsList.add( Convert.listOfPointsToMatOfPoints( polyMaskPoints ));
        fillPoly(polyMask, polyMaskPointsList , new Scalar(255,255,255,1));

        return polyMask;
    }

    public static Mat compositeMask( Mat base, Size blurSize ){

        ImageCorrector imgCorrector = new ImageCorrector();
        //CV_8UC1
        cvtColor(base, base, COLOR_BGRA2GRAY);
        base.convertTo(base, CvType.CV_8UC1);
        return imgCorrector.blurMat(base, blurSize);
    }

    public static Mat croppedByMask(Mat src, Mat mask){

        Mat output = Mat.zeros(src.size(), src.type());
        src.copyTo(output, mask);
        return output;
    }

    public static Mat grayscaleImage(Mat scr){

    Mat outputGray = new Mat(scr.size(), CvType.CV_8UC1);
    cvtColor(scr, outputGray, COLOR_BGRA2GRAY);

    cvtColor(outputGray, outputGray, COLOR_GRAY2BGRA);
    outputGray.convertTo(outputGray, scr.type());

        return outputGray;
    }

    public static Mat faceTextureMask(ArrayList<android.graphics.Point> pointsAvg, Mat imgBcg){

        FaceRegions faceRegions = new FaceRegions();

        // wszystkie operacje w przestrzeni: we -> CV_8UC4 -> CV_8UC1 -> wy CV_8UC4
        ImageCorrector imgCorrector = new ImageCorrector();

        Mat baseImg = Mat.zeros(imgBcg.size(), CvType.CV_8UC4);
        imgBcg.copyTo(baseImg);

        Mat outputMat = Mat.zeros(imgBcg.size(), CvType.CV_8UC1);

        Mat faceAndForeheadPolyMask = WMJmask.polyMask(faceRegions.faceAndForeheadIndexes(), pointsAvg, imgBcg.size());
        Mat faceAndForeheadPolyBlurred = imgCorrector.blurMat(faceAndForeheadPolyMask, new Size(65,65));
        Mat faceAndForeheadComposite = WMJmask.compositeMask(baseImg, new Size(32, 32));

        addWeighted(faceAndForeheadPolyBlurred, 0.5, faceAndForeheadComposite, 0.5, 0, outputMat);
        outputMat = imgCorrector.tresholdMat(outputMat,158,255,THRESH_TOZERO);

        Mat lowerAndMiddleFacePolyMask = WMJmask.polyMask(faceRegions.lowerAndMiddleFaceIndexes(), pointsAvg, imgBcg.size());
        Mat lowerAndMiddleFacePolyMaskBlurred = imgCorrector.blurMat(lowerAndMiddleFacePolyMask, new Size(32,32));

        addWeighted(outputMat, 1.0, lowerAndMiddleFacePolyMaskBlurred, 1.0, 0, outputMat);
        outputMat = WMJmask.croppedByMask(outputMat, faceAndForeheadPolyMask);

        /*
        //---szacowanie kształtu czoła:
        ArrayList<Point> foreheadUpperLinePoints = new ArrayList<>();
        for (int pointIndex : faceRegions.topFaceLineRightToLeft() ){

            Point point = pointsAvg.get(pointIndex);
            point.y = intUpTo(point.y, point.x, outputMat, 32);

            foreheadUpperLinePoints.add(point);
        }

        Mat detailedForeheadPolyMask = polyMask(foreheadUpperLinePoints,imgBcg.size());
        Mat detailedForeheadPolyMaskBlurred = blurMat(detailedForeheadPolyMask, new Size(32,32));
        detailedForeheadPolyMaskBlurred = croppedByMask(detailedForeheadPolyMaskBlurred,detailedForeheadPolyMask);
        //-----------------------------

        addWeighted(outputMat, 1.0, detailedForeheadPolyMaskBlurred, 1.0, 0, outputMat);

        */

        outputMat = imgCorrector.blurMat(outputMat, new Size(64,64));

        cvtColor(outputMat, outputMat, COLOR_GRAY2BGRA);
        outputMat.convertTo(outputMat, CvType.CV_8UC4);

        // outputMat = matWithTestPoints(foreheadUpperLinePoints,outputMat,new Scalar(0,255,0,1),4);

        return outputMat;

    }


}
