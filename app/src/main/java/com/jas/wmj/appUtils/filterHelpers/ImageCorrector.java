package com.jas.wmj.appUtils.filterHelpers;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.merge;
import static org.opencv.core.Core.minMaxLoc;
import static org.opencv.core.Core.split;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.core.CvType.CV_8UC4;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2BGRA;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2Lab;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2YCrCb;
import static org.opencv.imgproc.Imgproc.COLOR_BGRA2BGR;
import static org.opencv.imgproc.Imgproc.COLOR_Lab2BGR;
import static org.opencv.imgproc.Imgproc.COLOR_YCrCb2BGR;
import static org.opencv.imgproc.Imgproc.blur;
import static org.opencv.imgproc.Imgproc.createCLAHE;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.equalizeHist;
import static org.opencv.imgproc.Imgproc.filter2D;
import static org.opencv.imgproc.Imgproc.threshold;

/**
 * Created by WMJ on 2017-05-03.
 */


public class ImageCorrector {

    /*
    Zaimolementowane w JAVIE na podsawie:
    http://answers.opencv.org/question/75510/how-to-make-auto-adjustmentsbrightness-and-contrast-for-image-android-opencv-image-correction/
    */

    public Mat BrightnessAndContrastAuto(Mat src){

    Mat dst = new Mat( src.size(), src.type());

    int histSize = 256;
    double alpha, beta;
    double minGray , maxGray;

    Mat gray = new Mat( src.size(), src.type());
    if (src.type() == CV_8UC1) {gray = src;}
    else if (src.type() == CV_8UC3) {cvtColor(src, gray, Imgproc.COLOR_BGR2GRAY);}
    else if (src.type() == CV_8UC4) {cvtColor(src, gray, Imgproc.COLOR_BGRA2GRAY);}


    Core.MinMaxLocResult minMaxVal = minMaxLoc(gray);
    minGray = minMaxVal.minVal;
    maxGray = minMaxVal.maxVal;


    double inputRange = maxGray - minGray;

    alpha = (histSize - 1) / inputRange;   // alpha expands current range to histsize range
    beta = -minGray * alpha;             // beta shifts current range so that minGray will go to 0

    // Apply brightness and contrast normalization
    // convertTo operates with saurate_cast
        src.convertTo(dst, -1, alpha, beta);
    return dst;
}
// OpenCV's CLAHE (Contrast Limited Adaptive Histogram Equalization):
//http://stackoverflow.com/questions/24341114/simple-illumination-correction-in-images-opencv-c

    public Mat CLAHEfix( Mat src, double clipLimit, Size tilesGridSize  ){

        Mat dst = new Mat( src.size(), src.type());
        Mat lab_image = new Mat( src.size(), src.type());
        cvtColor(src, lab_image, COLOR_BGR2Lab );

        // Extract the L channel
        List<Mat> lab_planes = new ArrayList<>(3);
        split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]


        // apply the CLAHE algorithm to the L channel -> adaptive histogram equalization
        CLAHE clahe = createCLAHE();
        clahe.setClipLimit(clipLimit);//dydtrybucja tego
        clahe.setTilesGridSize( tilesGridSize ); //na tą powierzchnię
        clahe.apply(lab_planes.get(0), dst);


        // Merge the the color planes back into an Lab image
        dst.copyTo(lab_planes.get(0));
        merge(lab_planes, lab_image);

        // convert back to RGB
        Mat image_clahe = new Mat( src.size(), src.type());

        cvtColor(lab_image, image_clahe, COLOR_Lab2BGR );
        cvtColor(image_clahe, image_clahe, COLOR_BGR2BGRA );

        return image_clahe;
    }

    public Mat imageWithContrastAndBrightness(Mat src, double contrast, double brightness){

        Mat output = new Mat(src.size(), src.type());
        src.convertTo(output, -1, contrast, brightness);
        return output;
    }

    public Mat blurMat(Mat src, Size blurSize){

        Mat output = Mat.zeros(src.size(), src.type());
        blur(src, output, blurSize);
        return output;
    }

    public Mat tresholdMat(Mat src, int from, int to, int type){

        Mat output = new Mat(src.size(), src.type());
        threshold(src,output,from,to,type);
        return output;
    }

    public Scalar imageAverageColor4U(Mat scr){

        double r = 0;
        double g = 0;
        double b = 0;
        double alpha = 0;
        double numberOfPixels = 0;

        for (int row = 0; row < scr.rows(); row += 4){
                for (int column = 0; column < scr.cols(); column += 4){

                    double[] pixel = scr.get(row, column);
                    r = pixel[0] + r;
                    g = pixel[1] + g;
                    b = pixel[2] + b;
                    alpha = pixel[3] + alpha;
                    numberOfPixels++;
                }
        }

        if (numberOfPixels == 0){numberOfPixels = 1;}

        return new Scalar(
                r / numberOfPixels,
                g / numberOfPixels,
                b / numberOfPixels,
                alpha / numberOfPixels);
    }

    public double averageIluninationFromMat(Mat scr){

        double output = 0.0;
        double numberOfPixels = 0.0;

        Mat tempMat = new Mat();
        cvtColor(scr,tempMat, COLOR_BGRA2BGR);
        cvtColor(tempMat,tempMat, COLOR_BGR2YCrCb);

        List<Mat> channels = new ArrayList<>();
        split(tempMat,channels);

        for (int row = 0; row < scr.rows(); row += 4) {
            for (int column = 0; column < scr.cols(); column += 4) {

                double[] pixel = scr.get(row, column);
                output = pixel[0] + output;
                numberOfPixels++;
            }
        }

        if (numberOfPixels == 0){numberOfPixels = 1;}
        return output / numberOfPixels;
    }

    public Mat matWithIluminationFromMat(Mat output, Mat iluminationSource){

        Mat tempOutput = new Mat();
        cvtColor(output,tempOutput, COLOR_BGRA2BGR);
        cvtColor(tempOutput,tempOutput, COLOR_BGR2YCrCb);

        Mat tempIluminationSourcet = new Mat();
        cvtColor(iluminationSource,tempIluminationSourcet, COLOR_BGRA2BGR);
        cvtColor(tempIluminationSourcet,tempIluminationSourcet, COLOR_BGR2YCrCb);

        List<Mat> channelsOutput = new ArrayList<>();
        split(tempOutput,channelsOutput);

        List<Mat> channelsIluminationSource = new ArrayList<>();
        split(tempIluminationSourcet,channelsIluminationSource);

        channelsOutput.set(0, channelsIluminationSource.get(0));

        merge(channelsOutput,tempOutput);

        cvtColor(tempOutput,tempOutput, COLOR_YCrCb2BGR);
        cvtColor(tempOutput,tempOutput, COLOR_BGR2BGRA);

        return tempOutput;
    }

    public Mat equalizeHistInMat(Mat scr){

        Mat tempMat = new Mat();
        cvtColor(scr,tempMat, COLOR_BGRA2BGR);
        cvtColor(tempMat,tempMat, COLOR_BGR2YCrCb);

        List<Mat> channels = new ArrayList<>();
        split(tempMat,channels);

        Mat intensityChannel = new Mat(channels.get(0).size(), channels.get(0).type());
        equalizeHist(channels.get(0), intensityChannel);
        channels.set(0,intensityChannel);

        Mat output = new Mat(tempMat.size(), tempMat.type());
        merge(channels,output);

        cvtColor(output,output, COLOR_YCrCb2BGR);
        cvtColor(output,output, COLOR_BGR2BGRA);

        return output;
    }

}
