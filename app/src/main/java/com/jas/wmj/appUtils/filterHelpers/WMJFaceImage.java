package com.jas.wmj.appUtils.filterHelpers;

import android.graphics.Bitmap;
import android.graphics.Point;

import com.tzutalin.dlib.VisionDetRet;

import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WMJ on 2017-05-29.
 */

public class WMJFaceImage {

   // private List<VisionDetRet> _listOfLandmarks;
    private Bitmap _orgBmp;
    private Mat _orgMat;
    private ArrayList<Point> _points;
    private ArrayList< int[] > _delaunayTriangles;

    public WMJFaceImage(Bitmap orgBmp, Mat orgMat, ArrayList<Point> points, ArrayList< int[] > delaunayTriangles ){

        //_listOfLandmarks = listOfLandmarks;
        _orgBmp = orgBmp;
        _orgMat = orgMat;
        _points = points;
        _delaunayTriangles = delaunayTriangles;
    }


   // public List<VisionDetRet> getlistOfLandmarks(){return _listOfLandmarks; }
    public Bitmap getBmp(){return _orgBmp; }
    public Mat getMat() {return _orgMat; }
    public ArrayList<Point> getPoints(){ return _points; }
    public ArrayList< int[] > getDelaunayTriangles(){ return _delaunayTriangles; }

}
