package com.jas.wmj.appUtils.filterHelpers;

/**
 * Created by WMJ on 2017-05-05.
 */

public class FaceRegions {

    public int[][] getAllRegions(){
        int[][] allPoints = {

                this.rightUpperCheek(),//0
                this.leftUpperCheek(),//1
                this.faceAndForeheadIndexes(),//2
                this.lowerAndMiddleFaceIndexes(),//3
                this.lipsIndexes(),//4
                this.innerLipsIndexes(),//5
                this.noseIndexes(),//6
                this.shortMustacheIndexes(),//7
                this.longMustacheIndexes(),//8
                this.beardIndexes(),//9
                this.leftEye(),//10
                this.rightEye(),//11
                this.upperLeftEye(),//12
                this.upperRightEye(),//13
                this.leftCheek(),//14
                this.rightCheek(),//15
                this.lipsNoseAndEyes(),//16
                lipsForRegionId()//17
        };
        return allPoints;
    }

    public int[] faceAndForeheadIndexes(){//2
        int[] pointIndexes = {
                0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
                26,
                70,68,
                17
        };
        return pointIndexes;
    }

    public int[] lowerAndMiddleFaceIndexes(){//3
        int[] pointIndexes = {
                0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
                26,25,24,23,
                20,19,18,17
        };
        return pointIndexes;
    }

    public int[] lipsIndexes(){//4
        int[] pointIndexes = {
                60,49,50,51,52,53,64,55,56,57,58,59
        };
        return pointIndexes;
    }

    public int[] mouthIndexes(){
        int[] pointIndexes = {
                48,49,50,52,53,54,55,56,57,58,59,48
        };
        return pointIndexes;
    }

    public int[] innerLipsIndexes(){//5
        int[] pointIndexes = {
                67,61,62,63,65,66
        };
        return pointIndexes;
    }



    public int[] noseIndexes(){//7
        int[] pointIndexes = {
                31,32,33,34,35,
                27
        };
        return pointIndexes;
    }

    public int[] shortMustacheIndexes(){//8
        int[] pointIndexes = {
                48,49,50,51,52,53,54,
                35,34,33,32,31
        };
        return pointIndexes;
    }

    public int[] longMustacheIndexes(){//9
        int[] pointIndexes = {
                4,48,49,50,52,53,54,12,
                13,
                35,34,33,32,31,
                3
        };
        return pointIndexes;
    }

    public int[] beardIndexes(){//10
        int[] pointIndexes = {

                2,3,4,5,6,7,8,9,10,11,12,13,14,
                54,55,56,57,58,59,48
        };
        return pointIndexes;
    }

    public int[] leftEye(){//11
        int[] pointIndexes = {

                36,37,38,39,40,41
        };
        return pointIndexes;
    }

    public int[] rightEye(){//12
        int[] pointIndexes = {

                42,47,46,45,44,43
        };
        return pointIndexes;
    }

    public int[] upperLeftEye(){//13
        int[] pointIndexes = {

                17,18,19,20,21,
                39,38,37,36
        };
        return pointIndexes;
    }

    public int[] upperRightEye(){//14
        int[] pointIndexes = {

                22,23,24,25,26,
                45,44,43,42
        };
        return pointIndexes;
    }

    public int[] leftCheek(){//15
        int[] pointIndexes = {

                3,31,27,
                39,40,41,36,
                0,1,2
        };
        return pointIndexes;
    }

    public int[] rightCheek(){//16
        int[] pointIndexes = {

                27,35,13,
                14,15,16,
                45,46,47,42
        };
        return pointIndexes;
    }


    public int[] lipsNoseAndEyes(){//17
        int[] pointIndexes = {

                48,60,59,58,57,56,55,54,
                35,27,
                42,47,46,45,
                26,25,24,23,22,21,20,19,18,17,
                36,41,40,39,
                27,31,
                48,60,59,58,57,56,55
        };
        return pointIndexes;
    }

    public int[] topFaceLineLeftToRight(){
        int[] pointIndexes = {

                0,17,18,19,20,21,22,23,24,25,26,16
        };
        return pointIndexes;
    }

    public int[] topFaceLineRightToLeft(){
        int[] pointIndexes = {

                16,26,25,24,23,22,21,20,19,18,17,0
        };
        return pointIndexes;
    }

    public int[] lipsForRegionId(){//18
        int[] pointIndexes = {
                67,61,62,63,65,66,48,49,50,52,53,54,55,56,57,58,59,48
        };
        return pointIndexes;
    }

    public int[] rightUpperCheek(){
        int[] pointIndexes = {//1

                35,46,14,15
        };
        return pointIndexes;
    }

    public int[] leftUpperCheek(){ //0
        int[] pointIndexes = {

                31,41,1,2
        };
        return pointIndexes;
    }




}
