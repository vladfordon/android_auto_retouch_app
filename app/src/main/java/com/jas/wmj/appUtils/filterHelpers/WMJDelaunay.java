package com.jas.wmj.appUtils.filterHelpers;

import android.graphics.Point;
import com.jas.wmj.appUtils.Enums;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat6;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Subdiv2D;

import java.util.ArrayList;

import static java.lang.Math.abs;
import static org.opencv.core.Core.BORDER_REFLECT_101;
import static org.opencv.core.Core.transform;
import static org.opencv.imgproc.Imgproc.INTER_LINEAR;
import static org.opencv.imgproc.Imgproc.fillConvexPoly;
import static org.opencv.imgproc.Imgproc.getAffineTransform;
import static org.opencv.imgproc.Imgproc.warpAffine;
import static org.opencv.video.Video.estimateRigidTransform;

/**
 * Created by WMJ on 2017-05-16.
 */

public class WMJDelaunay {

    public ArrayList<Point> movedByNormalizationPointsArray(ArrayList<android.graphics.Point> arr, int ax, int by ){

        ArrayList<android.graphics.Point> newArr = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++){

            android.graphics.Point newPoint = new Point( arr.get(i).x + ax, arr.get(i).y + by );
            newArr.add(i,newPoint);
        }

        return newArr;
    }

    public Mat similarityTransform(android.graphics.Point[] inPoints, android.graphics.Point[] outPoints){

        double s60 = Math.sin(60*Math.PI/180);
        double c60 = Math.cos(60*Math.PI/180);


        int thirdPointInX = (int)(c60 * (inPoints[0].x - inPoints[1].x) - s60 * (inPoints[0].y - inPoints[1].y) + inPoints[1].x);
        int thirdPointInY = (int)(s60 * (inPoints[0].x - inPoints[1].x) + c60 * (inPoints[0].y - inPoints[1].y) + inPoints[1].y);

        Mat inPtsMat = new MatOfPoint(Convert.pointToOpenCVpoint(inPoints[0]), Convert.pointToOpenCVpoint(inPoints[1]), new org.opencv.core.Point( thirdPointInX, thirdPointInY ));

        int thirdPointOutX = (int)(c60 * (outPoints[0].x - outPoints[1].x) - s60 * (outPoints[0].y - outPoints[1].y) + outPoints[1].x);
        int thirdPointOutY = (int)(s60 * (outPoints[0].x - outPoints[1].x) + c60 * (outPoints[0].y - outPoints[1].y) + outPoints[1].y);

        Mat outPtsMat = new MatOfPoint(Convert.pointToOpenCVpoint(outPoints[0]), Convert.pointToOpenCVpoint(outPoints[1]), new org.opencv.core.Point( thirdPointOutX, thirdPointOutY ));

        return estimateRigidTransform(inPtsMat, outPtsMat, false);
    }

    public ArrayList< int[] > calculateDelaunayTriangles(Rect rect, ArrayList<android.graphics.Point> points){

        // Create an instance of Subdiv2D
        Subdiv2D subdiv = new Subdiv2D();
        subdiv.initDelaunay(rect);

        // Insert points into subdiv
        for (android.graphics.Point pt : points){
            subdiv.insert(new org.opencv.core.Point(pt.x, pt.y));
        }

        MatOfFloat6 triangleList = new MatOfFloat6();
        subdiv.getTriangleList(triangleList);

        //-------------------------------------------------------------------
        //# Find the indices (oznaki) of triangles in the points array
        ArrayList< int[] > delaunayTri = new ArrayList<>();

        org.opencv.core.Point[] pt;
        int[] indiceIndex;


        for ( int e = 0; e < triangleList.rows(); e++){//--------------------------------------------

            double[] t = triangleList.get(e,0);

            pt = new org.opencv.core.Point[3];
            pt[0] = new org.opencv.core.Point(t[0],t[1]);
            pt[1] = new org.opencv.core.Point(t[2],t[3]);
            pt[2] = new org.opencv.core.Point(t[4],t[5]);

            if (rect.contains(pt[0]) && rect.contains(pt[1]) && rect.contains(pt[2])){

                indiceIndex = new int[3];

                for(int j = 0; j < 3; j++){

                    for(int k = 0; k < points.size(); k++){

                        if(abs(pt[j].x - points.get(k).x) < 1.0 && abs(pt[j].y - points.get(k).y) < 1){ indiceIndex[j] = k; }

                    }
                }


                if (indiceIndex.length == 3){

                    delaunayTri.add(indiceIndex);
                }
            }
        }//------------------------------------------------------------------------------------------

        return delaunayTri;
    }

    public Mat imageFromWarpingTriangles(final Mat scr, final ArrayList< Triangle > inputTriangles, final ArrayList< Triangle > outputTriangles, ArrayList<Point> points  ){

        final Scalar blackColor = new Scalar(0,0,0,1);
        final Scalar whiteColor = new Scalar(255,255,255,1);
        final Scalar transparentColor = new Scalar(0,0,0,0);


        final Mat output = new Mat(scr.rows(), scr.cols(), scr.type(), blackColor );

        for (int c = 0; c < inputTriangles.size(); c++){

            Triangle inputTriangle = inputTriangles.get(c);
            Triangle outputTriangle = outputTriangles.get(c);

            //Ramki trójkątów:
            Rect inputTriangleBounds = inputTriangle.boundsFromTriangle();
            Rect outputTriangleBounds = outputTriangle.boundsFromTriangle();

            //Wycinanie ramki trójkąta z SCR oraz nadawanie współrzędnych trójkąta zgodnych z układem odniesienia wewnątrz ramki: ---
            Triangle inputTriangleCropped = inputTriangle.croppedTriangleFromTriangleAndBoundingBox(inputTriangleBounds);
            Triangle outputTriangleCropped = outputTriangle.croppedTriangleFromTriangleAndBoundingBox(outputTriangleBounds);

            Mat scrImageCropped = scr.submat(inputTriangleBounds);
            Mat outputImageCropped = new Mat(outputTriangleBounds.height, outputTriangleBounds.width, scr.type());
            //------------------------------------------------------------------------------------------------------------------------

            //Zniekształcenie ramki zawierającej trójkąt:
            Mat affineTransform = getAffineTransform(Convert.triangleToMatOfPoint2f(inputTriangleCropped), Convert.triangleToMatOfPoint2f(outputTriangleCropped));

            warpAffine(scrImageCropped, outputImageCropped, affineTransform, outputImageCropped.size(), INTER_LINEAR, BORDER_REFLECT_101, transparentColor);

            // Wygenerowanie oraz zastosowanie maski dla danego trójkąta:
            Mat mask = Mat.zeros(outputImageCropped.rows(), outputImageCropped.cols(), CvType.CV_8UC4);
            fillConvexPoly(mask, Convert.triangleToMatOfPoint(outputTriangleCropped), whiteColor);

            ImageCorrector imgCorrector = new ImageCorrector();
            FaceRegions allFaceRegions = new  FaceRegions();

            Enums.FaceRegion region = outputTriangle.getRegionFromArrayOfPoints( points,allFaceRegions.getAllRegions() );
            if (region == Enums.FaceRegion.lipsForRegionId ){
                mask = imgCorrector.blurMat(mask, new Size(3,3));
                outputImageCropped = imgCorrector.blurMat(outputImageCropped, new Size(2,2));
            }
            else if(region == Enums.FaceRegion.LeftUpperCheek || region == Enums.FaceRegion.RightUpperCheek){
                mask = imgCorrector.blurMat(mask, new Size(4,4));
                outputImageCropped = imgCorrector.blurMat(outputImageCropped, new Size(4,4));
            }

            outputImageCropped.copyTo( output.submat( outputTriangleBounds ), mask);
        }

        return output;
    }

    public ArrayList< Triangle > arrayOfTrianglesFromPointIndexArray( ArrayList< int[] > pointIndexArr, ArrayList< android.graphics.Point > pointsArr, int w, int h ){

        ArrayList< Triangle > output = new ArrayList<>();

        for ( int[] triangle : pointIndexArr ){

            output.add( new Triangle(
                    WMJGeometrics.constrainPoint( pointsArr.get( triangle[0]),w , h),
                    WMJGeometrics.constrainPoint( pointsArr.get( triangle[1]),w , h),
                    WMJGeometrics.constrainPoint( pointsArr.get( triangle[2]),w , h) )
            );
        }

        return output;
    }

    public double eyeScaleFactor(ArrayList<android.graphics.Point> listOfPointsFromOriginalImage,
                                 ArrayList<android.graphics.Point> listOfPointsFromFilterImage,
                                 Size originalImageSize,
                                 Size filterImageSize){

        double originalBitmapDstanceBetweenEyeCorners = WMJGeometrics.distanceBetweenPoints(listOfPointsFromOriginalImage.get(36), listOfPointsFromOriginalImage.get(45));
        double filterBitmapDstanceBetweenEyeCorners = WMJGeometrics.distanceBetweenPoints(listOfPointsFromFilterImage.get(36), listOfPointsFromFilterImage.get(45));

        //obliczanie współczynnika skali pomiędzy oboma obrazami

        double maxFactor;
        double factor = originalBitmapDstanceBetweenEyeCorners / filterBitmapDstanceBetweenEyeCorners;

        if (originalImageSize.width > originalImageSize.height){
            maxFactor = originalImageSize.width / filterImageSize.width;
        }
        else {
            maxFactor = originalImageSize.height / filterImageSize.height;
        }

        if (factor > maxFactor){ factor = maxFactor; }
        return factor;
    }

    public android.graphics.Point[] boundaryPoints(int w, int h){

        android.graphics.Point[] boundaryPts = {
                new android.graphics.Point(0,0),
                new android.graphics.Point(w/2,0),
                new android.graphics.Point(w-1,0),
                new android.graphics.Point(w-1,h/2),
                new android.graphics.Point(w-1,h-1),
                new android.graphics.Point(w/2,h-1),
                new android.graphics.Point(0,h-1),
                new android.graphics.Point(0,h/2),
        };
        return boundaryPts;
    }

    public ArrayList<android.graphics.Point> listOfPointsTransformedByTransform(ArrayList<android.graphics.Point> listOfPoints, Mat tform){

        //transformcja punktów
        Mat listOfPointsFromFilterImageMat = Convert.listOfPointsToMat(listOfPoints);

        transform(listOfPointsFromFilterImageMat, listOfPointsFromFilterImageMat, tform);

        for (int i = 0; i < listOfPointsFromFilterImageMat.rows(); i++ ){
            listOfPointsFromFilterImageMat.get(i,0);
            listOfPoints.set(i, new Point((int)listOfPointsFromFilterImageMat.get(i,0)[0], (int)listOfPointsFromFilterImageMat.get(i,0)[1])  );
        }

        return listOfPoints;
    }

    public ArrayList<android.graphics.Point> listOfPointsWithBoundryPoints(ArrayList<android.graphics.Point> points, android.graphics.Point[] boundaryPts){

        if (points.size() < 76) { for (android.graphics.Point boundryPoint : boundaryPts) { points.add(boundryPoint);}}
        return points;
    }

    public Point eyeReferencePoint(ArrayList<android.graphics.Point> listOfPointsFromOriginalImage, ArrayList<android.graphics.Point> listOfPointsFromFilterImage, Size filterTextureSize, Size originalImageSize){

        double wDiff = abs(originalImageSize.width - filterTextureSize.width);
        double hDiff = abs(originalImageSize.height - filterTextureSize.height);
        return new Point((int)(wDiff * 0.5), (int)(hDiff * 0.5) );
    }
}
