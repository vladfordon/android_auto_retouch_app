package com.jas.wmj.appUtils.filterHelpers;

/**
 * Created by WMJ on 2017-05-25.
 */

public class WMJmath {

    public static double normalizedWithinLimit(double min, double max, double val){

        if (val < min){
            val = min;
            return val;
        }
        else if (val > max){
            val = max;
            return val;
        }

        return val;
    }
}
