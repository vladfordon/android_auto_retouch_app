package com.jas.wmj.appUtils;

/**
 * Created by WMJ on 2016-12-21.
 */

public class Enums {

    public enum DeviceOrientation {
        portrait,
        lsleft,
        upsidedown,
        lsright,
        unknown,
    }

    public enum Status {
        waiting,
        noFace,
        faceTimeOk,
        faceTimeNoSmile,
        tooManyFaces,
        unknown
    }

    public enum FaceShapeBlendType {
        WhiteMale,
        WhiteMaleStrong,
        WhiteFemale,
        WhiteFemaleStrong,
        BlackMale,
        BlackMaleStrong,
        AsianMale,
        AsianMaleStrong,
    }

    public enum FaceRegion {
        RightUpperCheek, //0
        LeftUpperCheek,//1
        FaceAndForeheadIndexes,//2
        LowerAndMiddleFaceIndexes,//3
        LipsIndexes,//4
        InnerLipsIndexes,//5
        NoseIndexes,//6
        ShortMustacheIndexes,//7
        LongMustacheIndexes,//8
        BeardIndexes,//9
        LeftEye,//10
        RightEye,//11
        UpperLeftEye,//12
        UpperRightEye,//13
        LeftCheek,//14
        RightCheek,//15
        LipsNoseAndEyes,//16
        lipsForRegionId,//17
        Unknown
    }

}
