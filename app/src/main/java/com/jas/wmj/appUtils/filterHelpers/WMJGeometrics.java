package com.jas.wmj.appUtils.filterHelpers;

import android.graphics.Point;

import org.opencv.core.Mat;

import java.util.ArrayList;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Created by WMJ on 2017-05-16.
 */

public class WMJGeometrics {

    public static ArrayList<Point> rescaledPointsArray(ArrayList<android.graphics.Point> arr, double scale ){

        ArrayList<android.graphics.Point> newArr = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++){

            android.graphics.Point newPoint = new Point((int)(arr.get(i).x * scale), (int)(arr.get(i).y * scale));
            newArr.add(i,newPoint);
        }

        return newArr;
    }

    public static ArrayList<android.graphics.Point> averagePoints(ArrayList<android.graphics.Point> input, ArrayList<android.graphics.Point> output){

        ArrayList<android.graphics.Point> result = new ArrayList<>();
        for (int c = 0; c < input.size(); c++ ){

            result.add(new android.graphics.Point(
                            (input.get(c).x + output.get(c).x)/2,
                            (input.get(c).y + output.get(c).y)/2
                    )
            );
        }
        return result;
    }

    public static double distanceBetweenPoints(android.graphics.Point pointA, android.graphics.Point pointB){

        double a = (double)pointA.x - (double)pointB.x;
        double b = (double)pointA.y - (double)pointB.y;
        return Math.sqrt( Math.pow(a,2) + Math.pow(b,2) );
    }

    public static android.graphics.Point constrainPoint(android.graphics.Point p, int w, int h){

        p.x = min(max( p.x, 0), (w - 1));
        p.y = min(max( p.y, 0), (h - 1));

        return p;
    }

    public static int intUpTo(int y, int x, Mat mat, int colorValueLimit){

        for (int c = y; c > 0; c--){

            if ( (int)mat.get(c,x)[0] < colorValueLimit ){ return c; }
        }

        return y;
    }

}
