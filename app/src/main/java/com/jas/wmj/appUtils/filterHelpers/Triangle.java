package com.jas.wmj.appUtils.filterHelpers;

import android.graphics.Point;
import android.util.Log;

import com.jas.wmj.appUtils.Enums;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import java.util.ArrayList;

import static org.opencv.imgproc.Imgproc.boundingRect;

/**
 * Created by WMJ on 2017-04-27.
 */

public class Triangle {


    private Point _p0, _p1, _p2;

    public Triangle( Point p0, Point p1, Point p2 ) {

        _p0 = p0;
        _p1 = p1;
        _p2 = p2;
    }

    public void setP0(Point p0){ _p0 = p0; }
    public void setP1(Point p1){ _p1 = p1; }
    public void setP2(Point p2){ _p2 = p2; }

    public Point getP0(){ return _p0; }
    public Point getP1(){ return _p1; }
    public Point getP2(){ return _p2; }


    public Rect boundsFromTriangle(){

        return boundingRect( new MatOfPoint( Convert.pointToOpenCVpoint(this.getP0()),
                Convert.pointToOpenCVpoint(this.getP1()),
                Convert.pointToOpenCVpoint(this.getP2())
        ));
    }

    public Triangle croppedTriangleFromTriangleAndBoundingBox(Rect box){

        return new Triangle(
                new Point(this.getP0().x - box.x, this.getP0().y - box.y  ),
                new Point(this.getP1().x - box.x, this.getP1().y - box.y  ),
                new Point(this.getP2().x - box.x, this.getP2().y - box.y  ));
    }

    public int[] getIndexsForPointsFromCollectionOfPoints(ArrayList<Point> points){

        int[] output = { indexOfPoint(_p0, points), indexOfPoint(_p1, points), indexOfPoint(_p2, points) };
        return output;
    }

    private int indexOfPoint(Point pt, ArrayList<Point> points){

        int i = 0;
        for (Point point : points){
            if (point.x == pt.x && point.y == pt.y){ return i; }
            i++;
        }
        return i;
    }
//------------------------------
    public Enums.FaceRegion getRegionFromArrayOfPoints(ArrayList<Point> points, int[][] allFaceRegions){

        int[] pointIndexes = this.getIndexsForPointsFromCollectionOfPoints(points);
        Enums.FaceRegion[] regionsEnum = Enums.FaceRegion.values();

        int regionIndex = 0;
        for (int[] region : allFaceRegions){

            if (containsInt(region, pointIndexes[0] ) && containsInt(region, pointIndexes[1]) && containsInt(region, pointIndexes[2])){
                return regionsEnum[regionIndex];
            }
            regionIndex++;
        }

        return Enums.FaceRegion.Unknown;
    }

    private boolean containsInt(int[]arr, int val){

        for (int element : arr){
            if (element == val){
                return true;
            }
        }

        return false;
    }



}
