package com.jas.wmj.appUtils.filterHelpers;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by WMJ on 2017-04-29.
 */

public class Convert {


    public static org.opencv.core.Point pointToOpenCVpoint(android.graphics.Point point){

        org.opencv.core.Point openCVpoint = new org.opencv.core.Point(point.x, point.y);
        return  openCVpoint;
    }

    public static android.graphics.Point openCVpointTopoint(org.opencv.core.Point openCVpoint){

        android.graphics.Point point = new android.graphics.Point((int)openCVpoint.x, (int)openCVpoint.y);
        return  point;
    }

    public static ArrayList<Point> arrayOfpointsToArrayOfOpenCVPoints(ArrayList<android.graphics.Point> input){

        ArrayList<org.opencv.core.Point> output = new ArrayList();

        for (int i = 0; i < input.size(); i++){

            android.graphics.Point point = input.get(i);
            output.add(Convert.pointToOpenCVpoint(point));
        }

        return output;
    }

    public static Mat listOfPointsToMat(ArrayList<android.graphics.Point> listOfPoints){

        org.opencv.core.Point[] listOfPointsArray =
                Convert.arrayOfpointsToArrayOfOpenCVPoints(listOfPoints).toArray(new org.opencv.core.Point[listOfPoints.size()]);
        return new MatOfPoint(listOfPointsArray);
    }

    public static ArrayList<android.graphics.Point> arrayOfOpenCVPointsToArrayOfoints(ArrayList<org.opencv.core.Point> input){

        ArrayList<android.graphics.Point> output = new ArrayList();

        for (int i = 0; i < input.size(); i++){

            org.opencv.core.Point point = input.get(i);
            output.add(new android.graphics.Point( (int)point.x, (int)point.y) );
        }

        return output;
    }

    public static MatOfPoint listOfPointsToMatOfPoints(ArrayList<android.graphics.Point> listOfPoints){

        org.opencv.core.Point[] listOfPointsArray =
                Convert.arrayOfpointsToArrayOfOpenCVPoints(listOfPoints).toArray(new org.opencv.core.Point[listOfPoints.size()]);
        return new MatOfPoint(listOfPointsArray);
    }

    public static MatOfPoint2f listOfPointsToMatOfPoints2f(ArrayList<android.graphics.Point> listOfPoints){

        org.opencv.core.Point[] listOfPointsArray =
                Convert.arrayOfpointsToArrayOfOpenCVPoints(listOfPoints).toArray(new org.opencv.core.Point[listOfPoints.size()]);
        return new MatOfPoint2f(listOfPointsArray);
    }

    public static MatOfPoint2f triangleToMatOfPoint2f(Triangle tri) {

        return new MatOfPoint2f(
                                new org.opencv.core.Point( tri.getP0().x, tri.getP0().y),
                                new org.opencv.core.Point( tri.getP1().x, tri.getP1().y),
                                new org.opencv.core.Point( tri.getP2().x, tri.getP2().y)
                                );
    }

    public static MatOfPoint triangleToMatOfPoint(Triangle tri) {

        return new MatOfPoint(
                new org.opencv.core.Point( tri.getP0().x, tri.getP0().y),
                new org.opencv.core.Point( tri.getP1().x, tri.getP1().y),
                new org.opencv.core.Point( tri.getP2().x, tri.getP2().y)
        );
    }

}
