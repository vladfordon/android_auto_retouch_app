package com.jas.wmj.appUtils;

import android.app.Application;
import android.graphics.Bitmap;

import com.tzutalin.dlib.PeopleDet;
import com.tzutalin.dlib.VisionDetRet;

import java.util.List;


/**
 * Created by WMJ on 2016-12-10.
 */

public class MainSingleton extends Application {

    private static MainSingleton mInstance;
    private boolean _didPlayIntroTTS;
    private boolean _didplayEditTTSIntro;

    private PeopleDet landmarkDetector;
    private List<VisionDetRet> originalImageLandmarks;

    private static String preferencesName = "wmjPhotoAppPreferences";

    private Bitmap _mRGBbitmap;

    private double _originalImageAvgIllumination;


    @Override
    public void onCreate() {

        mInstance = this;
        _didplayEditTTSIntro = false;
        _didPlayIntroTTS = false;
    }

    public static synchronized MainSingleton getInstance() {
        return mInstance;
    }
    //-----

    public String getPrefName() { return this.preferencesName; }

    public double getOriginalImageAvgIllumination(){ return  this._originalImageAvgIllumination; }
    public void setOriginalImageAvgIllumination(double val){ this._originalImageAvgIllumination = val;}

    public boolean getDidPlayIntroTTS() { return this._didPlayIntroTTS; }
    public void setDidPlayIntroTTS(boolean didPlayIntroTTS) { this._didPlayIntroTTS = didPlayIntroTTS; }


    public boolean getDidplayEditTTSIntro() { return this._didplayEditTTSIntro; }
    public void setDidplayEditTTSIntro(boolean didPlayIntroEditTTS) { this._didplayEditTTSIntro = didPlayIntroEditTTS;}


    public List<VisionDetRet> getOriginalImageLandmarks() { return this.originalImageLandmarks; }
    public void setOriginalImageLandmarks(List<VisionDetRet> _originalImageLandmarks) { this.originalImageLandmarks = _originalImageLandmarks;}


    public PeopleDet getLandmarkDetector() { return this.landmarkDetector; }
    public void setLandmarkDetector(PeopleDet _landmarkDetector) { this.landmarkDetector = _landmarkDetector;}

    public Bitmap getmRGBbitmap(){ return this._mRGBbitmap; }
    public void setmRGBbitmap(Bitmap bmp){ this._mRGBbitmap = bmp; }
}
