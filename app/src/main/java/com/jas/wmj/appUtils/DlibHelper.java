package com.jas.wmj.appUtils;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;

import com.tzutalin.dlib.Constants;
import com.tzutalin.dlib.VisionDetRet;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WMJ on 2017-01-19.
 */

public class DlibHelper {

    public static List<VisionDetRet> landmarksFromBitmap(Bitmap bmp){

        Mat tempMat = new Mat (bmp.getWidth(),bmp.getHeight(), CvType.CV_8UC1);
        Utils.bitmapToMat(bmp,tempMat);

        Bitmap grayBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.RGB_565);

        Imgproc.cvtColor(tempMat, tempMat, Imgproc.COLOR_RGB2GRAY);
        Utils.matToBitmap(tempMat,grayBitmap);


        List<VisionDetRet>list = MainSingleton.getInstance().getLandmarkDetector().detBitmapFace(grayBitmap, Constants.getFaceShapeModelPath());
        return list;
    }


    public static Bitmap bitmapWithVisibleLandmarks(Bitmap bmp, List<VisionDetRet> bmp_Landmarks){

        Mat tempMat = new Mat (bmp.getWidth(),bmp.getHeight(),CvType.CV_8SC4);
        Utils.bitmapToMat(bmp,tempMat);
        final Scalar lineColor = new Scalar(255, 255, 255);

        for (final VisionDetRet ret : bmp_Landmarks) {

            // Rysowanie landmarków

            ArrayList<Point> landmarks = ret.getFaceLandmarks();
            for (android.graphics.Point point : landmarks) {

                Imgproc.circle(tempMat, new org.opencv.core.Point(point.x, point.y), 2, lineColor);
            }
        }

        Utils.matToBitmap(tempMat,bmp);
        return bmp;
    }


    public static ArrayList<android.graphics.Point> listOfPointsFromLandmarks(List<VisionDetRet> list){

        VisionDetRet ret = list.get(0);
        return ret.getFaceLandmarks();
    }
}
