package com.jas.wmj.appUtils;

/**
 * Created by WMJ on 2016-11-16.
 */
import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import com.tzutalin.dlib.Constants;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by darrenl on 2016/3/30.
 */
public class FileUtils {
    private static final String TAG = "com.jas.wmj.dlibUtils";

    @NonNull
    public static final void copyFileFromRawToOthers(@NonNull final Activity context, @RawRes int id, @NonNull final String targetPath) {
        InputStream in = context.getResources().openRawResource(id);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(targetPath);
            byte[] buff = new byte[1024];
            int read = 0;
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void saveBitmap(final Bitmap bitmap, String fname) {

        final File file = new File(Constants.appPath()+ File.separator, fname);

        if (file.exists()) {
            file.delete();
        }

        try {
            final FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveMatAsBitmap(Mat mat , String bitmapName)
    {
        Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(mat, bmp);
        FileUtils.saveBitmap(bmp, bitmapName);
    }





}