package com.jas.wmj.Filters;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;

import com.jas.wmj.appUtils.Enums;
import com.jas.wmj.appUtils.filterHelpers.Convert;
import com.jas.wmj.appUtils.filterHelpers.ImageCorrector;
import com.jas.wmj.appUtils.filterHelpers.Triangle;
import com.jas.wmj.appUtils.DlibHelper;
import com.jas.wmj.appUtils.filterHelpers.WMJDelaunay;
import com.jas.wmj.appUtils.filterHelpers.WMJFaceImage;
import com.jas.wmj.appUtils.filterHelpers.WMJGeometrics;
import com.jas.wmj.appUtils.filterHelpers.WMJImageLayerGenerator;
import com.jas.wmj.appUtils.filterHelpers.WMJmask;
import com.jas.wmj.appUtils.filterHelpers.WMJmath;
import com.tzutalin.dlib.VisionDetRet;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.BORDER_CONSTANT;
import static org.opencv.core.Core.addWeighted;
import static org.opencv.imgproc.Imgproc.INTER_LINEAR;
import static org.opencv.imgproc.Imgproc.line;
import static org.opencv.imgproc.Imgproc.warpAffine;

/**
 * Created by WMJ on 2017-01-11.
 */

public class AverageBitmap {
    
    private Bitmap _originalBmp;
    private Bitmap _filterBmp;
    private List<VisionDetRet> _originalBmp_Landmarks;
    private List<VisionDetRet> _filterBmp_Landmarks;
    private double _avgIluminationOriginal;
    private Enums.FaceShapeBlendType _blendType;

    private boolean _debug;


    public AverageBitmap(Bitmap originalBmp,
                         Bitmap filterBmp,
                         List<VisionDetRet> bmpA_Landmarks,
                         List<VisionDetRet> bmpB_Landmarks,
                         double avgIluminationOriginal,
                         Enums.FaceShapeBlendType blendType,
                         boolean debugMode
    ) {

        _originalBmp = originalBmp;
        _filterBmp = filterBmp;
        _originalBmp_Landmarks = bmpA_Landmarks;
        _filterBmp_Landmarks = bmpB_Landmarks;
        _avgIluminationOriginal = avgIluminationOriginal;
        _blendType = blendType;
        _debug = debugMode;
    }

    //metoda główna:
    public WMJFaceImage averageBitmapWithBitmap() {

        ImageCorrector imgCorrector = new ImageCorrector();
        WMJDelaunay delaunayManager = new WMJDelaunay();
        WMJImageLayerGenerator layerGen = new WMJImageLayerGenerator();

        //pozyskanie parametrów: tablice 68 punktów dla zdjęcia oraz tekstury filtra, odległoi pomiędzy krańcami oczu na obu obrazach
        ArrayList<android.graphics.Point> listOfPointsFromOriginalImage = DlibHelper.listOfPointsFromLandmarks(_originalBmp_Landmarks);
        ArrayList<android.graphics.Point> listOfPointsFromFilterImage = DlibHelper.listOfPointsFromLandmarks(_filterBmp_Landmarks);

        // 36 = kraniec lewego oka
        // 45 = kraniec prawego oka

        //obliczanie współczynnika skali pomiędzy oboma obrazami
        double scaleFactorToOriginal = delaunayManager.eyeScaleFactor(
                listOfPointsFromOriginalImage,
                listOfPointsFromFilterImage,
                new Size(_originalBmp.getWidth(), _originalBmp.getHeight()),
                new Size(_filterBmp.getWidth(), _filterBmp.getHeight())
        );

        //-------------------- Normalizacja układu odniesienia #1 ------------------------------------------------

        listOfPointsFromFilterImage = WMJGeometrics.rescaledPointsArray(listOfPointsFromFilterImage, scaleFactorToOriginal);

        //uwaga: H/W !!! //konwersja oryginalnego zdjęcia do postaci macierzy OpenCV
        Mat originalImageMat = new Mat(_originalBmp.getHeight(), _originalBmp.getWidth(), CvType.CV_8UC4);
        Utils.bitmapToMat(_originalBmp,originalImageMat);

        // współrzędne punktu odniesienia wg. którego będziemy nakładać ramkę
        Size filterTextureSize = new Size((double)_filterBmp.getWidth() * scaleFactorToOriginal, (double)_filterBmp.getHeight() * scaleFactorToOriginal);
        
        Point referencePoint = delaunayManager.eyeReferencePoint(listOfPointsFromOriginalImage, listOfPointsFromFilterImage, filterTextureSize, originalImageMat.size() );

        //przesunięcie punktów
        listOfPointsFromFilterImage = delaunayManager.movedByNormalizationPointsArray(
                listOfPointsFromFilterImage,
                referencePoint.x,
                referencePoint.y);

        //Utworzenie tekstury filtra:
        Mat filterCanvasMat = layerGen.filerCanvas(referencePoint.x, referencePoint.y, scaleFactorToOriginal, CvType.CV_8UC4, _filterBmp, originalImageMat.size() );

        //ona skutek noramlizacji otrzymujemy - OUT:
        // Mat -> originalImageMat -> oryginalne zdjęcie
        // Mat -> filterCanvasMat -> znormalizowana tekstura filtra, gdzie twarz pokrywa się 'mniej więcej' z twarzą na zdjęciu oryginalnym
        //
        // ArrayList<android.graphics.Point> -> listOfPointsFromOriginalImage -> oryginalne zdjęcie (lista 68 elementów)
        // ArrayList<android.graphics.Point> listOfPointsFromFilterImage -> znormalizowana tekstura filtra(lista 68 elementów)
        //
        //========================================================================================================

        int w = originalImageMat.cols();
        int h = originalImageMat.rows();

        android.graphics.Point[] eyecornerDst = {listOfPointsFromOriginalImage.get(36),listOfPointsFromOriginalImage.get(45)};
        android.graphics.Point[] eyecornerSrc = {listOfPointsFromFilterImage.get(36),listOfPointsFromFilterImage.get(45)};

        // punkty graniczne dla triangulacji Delaunay-a
        android.graphics.Point[] boundaryPts = delaunayManager.boundaryPoints(w, h);

        //Wygenerowanie transformaty podobieństwa
        Mat  tform = delaunayManager.similarityTransform(eyecornerSrc, eyecornerDst);

        // Zastosowanie transformaty podobieństwa w celu dopasowania kąta nachylenia oraz dalszego dopasowania skali teksury filtra
        warpAffine(filterCanvasMat,
                   filterCanvasMat,
                   tform,
                   filterCanvasMat.size(),INTER_LINEAR,BORDER_CONSTANT, layerGen.cornerPixelColorFromMat( filterCanvasMat ));

        listOfPointsFromFilterImage = delaunayManager.listOfPointsTransformedByTransform(listOfPointsFromFilterImage,tform);
        
        ArrayList<android.graphics.Point> pointsAvg = WMJGeometrics.averagePoints(listOfPointsFromFilterImage, listOfPointsFromOriginalImage);

        //--------------------------------------------------------------------------------------------------------
        // Triangulacja Delaunay-a

        // Dodanie 8 punktów granicznych w celu przeprowadzenia triangulacji Delaunay-a
        listOfPointsFromOriginalImage = delaunayManager.listOfPointsWithBoundryPoints( listOfPointsFromOriginalImage, boundaryPts );
        
        listOfPointsFromFilterImage = delaunayManager.listOfPointsWithBoundryPoints(listOfPointsFromFilterImage, boundaryPts);
       
        pointsAvg = delaunayManager.listOfPointsWithBoundryPoints(pointsAvg, boundaryPts);

        //--
        Mat[] imagesNorm = {originalImageMat, filterCanvasMat};

        ArrayList< ArrayList<android.graphics.Point> > pointsNorm = new ArrayList<>();
        pointsNorm.add(listOfPointsFromOriginalImage);
        pointsNorm.add(listOfPointsFromFilterImage);

        Rect rect = new Rect(0,0,w,h);

        ArrayList< int[] > dt = delaunayManager.calculateDelaunayTriangles(rect, pointsAvg); //ok

        // Obraz końcowy
        Mat outputMat = new Mat(h, w, CvType.CV_8UC4);

        ArrayList<Triangle> outputTriangles = delaunayManager.arrayOfTrianglesFromPointIndexArray(dt, pointsAvg, w, h);

        ArrayList< Triangle > originalImageTriangles = delaunayManager.arrayOfTrianglesFromPointIndexArray(dt, pointsNorm.get(0), w, h);
        ArrayList< Triangle > filterImageTriangles = delaunayManager.arrayOfTrianglesFromPointIndexArray(dt, pointsNorm.get(1), w, h);

        ArrayList< ArrayList< Triangle > > trianglesInImages = new ArrayList<>();
        trianglesInImages.add(originalImageTriangles);
        trianglesInImages.add(filterImageTriangles);


        for (int c = 0; c < 2; c++){

            imagesNorm[c] = delaunayManager.imageFromWarpingTriangles(imagesNorm[c], trianglesInImages.get(c), outputTriangles, pointsAvg);
        }

        double blendLimit = 0.25;
        double blendLimitMin = 0.1;
        double filterTextureBlendValue = _avgIluminationOriginal * 0.0025; //im ciemniej tym mniejszy blend
        double blendIluminationFactor = blendLimit;

        switch (_blendType) {
            case WhiteMale: //ok (def)
                break;

            case WhiteMaleStrong: //ok
                filterTextureBlendValue = 1.5 * filterTextureBlendValue;
                blendLimitMin = 0.25;
                blendLimit = 0.4;
                blendIluminationFactor = 0.85 * (1.0 - WMJmath.normalizedWithinLimit(blendLimitMin, blendLimit, filterTextureBlendValue));
                break;

            case WhiteFemale://ok
                filterTextureBlendValue = 2 * filterTextureBlendValue;
                blendIluminationFactor = 0.75 * WMJmath.normalizedWithinLimit(blendLimitMin, blendLimit, filterTextureBlendValue);
                break;

            case WhiteFemaleStrong://ok (def)
                filterTextureBlendValue = 0.4;
                blendLimit = filterTextureBlendValue;
                blendIluminationFactor = WMJmath.normalizedWithinLimit(blendLimitMin, blendLimit, filterTextureBlendValue);
                break;

            case BlackMale: //ok (def)
                filterTextureBlendValue = 2.0 * filterTextureBlendValue;
                blendLimitMin = 0.2;
                blendLimit = 0.4;
                blendIluminationFactor = blendLimit;
                break;

            case BlackMaleStrong: //-ok
                filterTextureBlendValue = 0.4 + filterTextureBlendValue;
                blendLimitMin = 0.46;
                blendLimit = 0.65;
                blendIluminationFactor = 0.75 * WMJmath.normalizedWithinLimit(blendLimitMin, blendLimit, filterTextureBlendValue);
                break;

            case AsianMale: //ok (def)
                filterTextureBlendValue = 0.4 * filterTextureBlendValue;
                break;

            case AsianMaleStrong: //ok
                filterTextureBlendValue = 1.25 * filterTextureBlendValue;
                blendLimitMin = 0.3;
                blendLimit = 0.46;
                blendIluminationFactor = blendLimit;
                break;
        }

        filterTextureBlendValue = WMJmath.normalizedWithinLimit(blendLimitMin,blendLimit,filterTextureBlendValue);

        double backgroundTextureBlendValue = 0.9 - filterTextureBlendValue;
        addWeighted(imagesNorm[0], backgroundTextureBlendValue, imagesNorm[1], filterTextureBlendValue, 0, outputMat);

        //--------------------------------------------------------------------------------------------------------
        //test:
        //  outputMat = matWithTestPoints(pointsNorm.get(0),outputMat,new Scalar(255,0,0,1),2); //org
         // outputMat = matWithTestPoints(pointsNorm.get(1),outputMat,new Scalar(0,0,255,1),6);   // filtr
         // outputMat = matWithTestPoints(testXXX,outputMat,new Scalar(0,255,0,1),4);   // avr

        //ArrayList<Point> xy = new ArrayList<>();
        // xy.add(new Point((int) center.x, (int) center.y));
        // outputMat = matWithTestPoints(xy, outputMat,new Scalar(0,255,255,1),8);   //

        //--------------------------------------------------------------------------------------------------------


        double newIlluminationVal = imgCorrector.averageIluninationFromMat(outputMat);
        double contrast = 1.0;
        outputMat = imgCorrector.imageWithContrastAndBrightness(outputMat, contrast ,_avgIluminationOriginal - newIlluminationVal);

        outputMat = imgCorrector.CLAHEfix(outputMat, 0.5, new Size(16,16));
        outputMat = imgCorrector.BrightnessAndContrastAuto(outputMat);


        addWeighted(imagesNorm[0], blendIluminationFactor, outputMat, 1 - blendIluminationFactor, 0, outputMat);
        outputMat = imgCorrector.BrightnessAndContrastAuto(outputMat);

        Mat outputGray = WMJmask.grayscaleImage(outputMat);
        outputGray = imgCorrector.blurMat(outputGray, new Size(64,64));

        addWeighted(outputGray, blendIluminationFactor * 0.25, outputMat, 1 - blendIluminationFactor * 0.25, 0, outputMat);

        if(_blendType == Enums.FaceShapeBlendType.BlackMaleStrong ){

            double contrastSpectrum = 0.125;
            double contrastFactor = filterTextureBlendValue / blendLimit;
            contrast = 1.0 + contrastSpectrum * contrastFactor * 0.6;
            contrast =  WMJmath.normalizedWithinLimit(1.0, 1.0 + contrastSpectrum ,contrast);

            outputMat = imgCorrector.imageWithContrastAndBrightness(outputMat, contrast ,0);
        }


        if (_debug) {
            outputMat = matWithTestTriangles(trianglesInImages.get(0), outputMat, new Scalar(255, 0, 0, 1), 2);  // tri org
            outputMat = matWithTestTriangles(trianglesInImages.get(1), outputMat, new Scalar(0, 255, 0, 1), 2);  // tri filtr
            outputMat = matWithTestTriangles(outputTriangles, outputMat, new Scalar(0, 0, 255, 1), 2);  // tri output
        }

        //uwaga: W/H !!!
        Bitmap outputBmp = Bitmap.createBitmap(outputMat.cols(), outputMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(outputMat, outputBmp);
        return new WMJFaceImage(outputBmp, outputMat, pointsAvg, dt);
    }




//---------------------



    public WMJFaceImage faceImageWithAddedAccessories(WMJFaceImage input, Bitmap accesoryTexture, ArrayList<Point> accessoryPoints){

        ImageCorrector imgCorrector = new ImageCorrector();
        WMJDelaunay delaunayManager = new WMJDelaunay();
        WMJImageLayerGenerator layerGen = new WMJImageLayerGenerator();

        Scalar transparentColor = new Scalar(0,0,0,0);

        ArrayList<Point> outputPoints = input.getPoints();

        double scaleFactorToOriginal = delaunayManager.eyeScaleFactor(
                outputPoints,
                accessoryPoints,
                input.getMat().size(),
                new Size(accesoryTexture.getWidth(), accesoryTexture.getHeight())
        );
        accessoryPoints = WMJGeometrics.rescaledPointsArray(accessoryPoints, scaleFactorToOriginal);

        Mat inputMat = input.getMat();

        // współrzędne odległości przesunięcia dla punktów oraz tekstury filtra
        Size filterTextureSize = new Size((double)accesoryTexture.getWidth() * scaleFactorToOriginal, (double)accesoryTexture.getHeight() * scaleFactorToOriginal);
        Point referencePoint = delaunayManager.eyeReferencePoint(outputPoints, accessoryPoints, filterTextureSize, inputMat.size());

        //przesunięcie punktów
        accessoryPoints = delaunayManager.movedByNormalizationPointsArray(
                accessoryPoints,
                referencePoint.x,
                referencePoint.y);

        Mat filterMat = layerGen.filerCanvas(referencePoint.x, referencePoint.y, scaleFactorToOriginal, inputMat.type(), accesoryTexture, inputMat.size() );

        //========================================================================================================

        int w = inputMat.cols();
        int h = inputMat.rows();

        android.graphics.Point[] eyecornerDst = {outputPoints.get(36),outputPoints.get(45)};
        android.graphics.Point[] eyecornerSrc = {accessoryPoints.get(36),accessoryPoints.get(45)};

        // punkty graniczne dla triangulacji Delaunay-a
        android.graphics.Point[] boundaryPts = delaunayManager.boundaryPoints(w, h);

        //Wygenerowanie transformaty podobieństwa
        Mat  tform = delaunayManager.similarityTransform(eyecornerSrc, eyecornerDst);

        // Zastosowanie transformaty podobieństwa w celu dopasowania kąta nachylenia oraz dalszego dopasowania skali teksury filtra
        warpAffine(filterMat,
                filterMat,
                tform,
                filterMat.size(),INTER_LINEAR,BORDER_CONSTANT, transparentColor);

        accessoryPoints = delaunayManager.listOfPointsTransformedByTransform(accessoryPoints,tform);
        //--------------------------------------------------------------------------------------------------------
        // Triangulacja Delaunay-a

        // Dodanie 8 punktów granicznych w celu przeprowadzenia triangulacji Delaunay-a
        accessoryPoints = delaunayManager.listOfPointsWithBoundryPoints(accessoryPoints, boundaryPts);

        ArrayList< int[] > dt = input.getDelaunayTriangles();

        // Obraz końcowy
        Mat outputMat = new Mat(inputMat.size(), inputMat.type());

        ArrayList<Triangle> outputTriangles = delaunayManager.arrayOfTrianglesFromPointIndexArray(dt, outputPoints, w, h);
        ArrayList< Triangle > filterImageTriangles = delaunayManager.arrayOfTrianglesFromPointIndexArray(dt, accessoryPoints, w, h);

        filterMat = delaunayManager.imageFromWarpingTriangles(filterMat, filterImageTriangles, outputTriangles, outputPoints);

        //--- blend

        inputMat.copyTo(outputMat);

        int margin = 48;
        int faceRectY = outputPoints.get(19).y - margin ;
        if (faceRectY < 1){faceRectY = 1;}

        int faceRectX = outputPoints.get(0).x - margin ;
        if (faceRectX < 1){faceRectX = 1;}

        int faceRectWidth = (int)WMJGeometrics.distanceBetweenPoints(outputPoints.get(0), outputPoints.get(16)) + margin * 2;
        faceRectWidth = accessoryMatRenderWindowOptimizedDimension(faceRectWidth, faceRectX, outputMat.width() );

        int faceRectHeight = (int)WMJGeometrics.distanceBetweenPoints(outputPoints.get(19), outputPoints.get(8)) + margin * 2;
        faceRectHeight = accessoryMatRenderWindowOptimizedDimension(faceRectHeight, faceRectY, outputMat.height() );

        //------------

        double hairContrast = 0.85;
        double addBrightnesToHair = 0.0;

        double blendFactor = 0.85;
        double blendTreshold = 32.0;

        double claheTreshold = 0.5;
        Size claheArea = new Size(2,2);


        filterMat = imgCorrector.imageWithContrastAndBrightness(filterMat, hairContrast, addBrightnesToHair);

        Rect faceRect = new Rect(faceRectX, faceRectY, faceRectWidth, faceRectHeight);

        outputMat = layerGen.imageWithRGBAoverlay(inputMat, filterMat, blendFactor, faceRect, blendTreshold); //gdy są jasne piksele jest prawie ok
        outputMat = imgCorrector.CLAHEfix(outputMat, claheTreshold, claheArea);

        //---------

        Bitmap outputBmp = Bitmap.createBitmap(outputMat.cols(), outputMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(outputMat, outputBmp);

        return new WMJFaceImage(outputBmp, outputMat, outputPoints, dt);
    }


    private int accessoryMatRenderWindowOptimizedDimension(int faceRectDimension, int faceRectStartingPointXorY, int outputMatDimension){

        if (faceRectDimension + faceRectStartingPointXorY > outputMatDimension){

            int r = (outputMatDimension - faceRectDimension - faceRectStartingPointXorY) * -1;
            faceRectDimension = faceRectDimension - r;
        }
        if (faceRectDimension < 0){faceRectDimension = 0;}

        return faceRectDimension;
    }


    //dla testów:
    private Mat matWithTestPoints(ArrayList<android.graphics.Point> arr, Mat image,Scalar color, int thickness){

        for (int i = 0; i < arr.size(); i++){
            Imgproc.circle(image, Convert.pointToOpenCVpoint(arr.get(i)), thickness, color);
        }

        return image;
    }

    private Mat matWithTestTriangles( ArrayList< Triangle > triangleArr, Mat image,Scalar color, int thickness ){


        for ( Triangle triangle : triangleArr ){

            line( image, Convert.pointToOpenCVpoint( triangle.getP0() ) , Convert.pointToOpenCVpoint( triangle.getP1() ), color, thickness );
            line( image, Convert.pointToOpenCVpoint( triangle.getP1() ) , Convert.pointToOpenCVpoint( triangle.getP2() ), color, thickness );
            line( image, Convert.pointToOpenCVpoint( triangle.getP0() ) , Convert.pointToOpenCVpoint( triangle.getP2() ), color, thickness );

        }

        return image;
    }
}
