package com.tzutalin.dlib;

import android.os.Environment;

import java.io.File;

/**
 * Created by darrenl on 2016/4/22.
 */
public final class Constants {
    private Constants() {
        // Constants should be prive
    }

    /**
     * getFaceShapeModelPath
     * @return default face shape model path
     */
    public static String getFaceShapeModelPath() {
         return Constants.appPath() + File.separator +"shape_predictor_68_face_landmarks.dat";
    }

    public static String appPath()
    {
        File sdcard = Environment.getExternalStorageDirectory();
        return sdcard.getAbsolutePath() + File.separator +"0_wmj_PracaDyplApp";
    }

    public static String tempName()
    {
        return "imgTemp";
    }

}
